# Software Engineering Project - Skull King

By Berisha Pjeter, Bottoni Paolo, Flück Sunniva, Humbel Ciril, Rousseau Clément, Vogel Pascal

Link to project [website](https://gitlab.ethz.ch/vogelpa/software-engineering-group-project). The game can be found on the main branch, other branches are for experimental changes/additions.

Disclaimer: Some parts, such as part of the preparation of the OS environment are directly taken from the provided ReadMe of the Lama game.

This is a simple C++ implementation of the game "Skull King" by Schmidt Spiele. You can read the game's rules as soon as you start the game in the top panel. The implementation features a client/server architecture for multiplayer scenarios, which can be accessed from different consoles.
It uses [wxWidgets](https://www.wxwidgets.org/) for the GUI, [sockpp](https://github.com/fpagliughi/sockpp) for the network interface, [rapidjson](https://rapidjson.org/md_doc_tutorial.html) for object serialization, and [googletest](https://github.com/google/googletest) for the unit tests. 

![SkullKing-logo](./code/assets/Skull-King-logo.jpg)


## 1. Compile instructions
**Note:** If you create a virtual machine, we recommend giving the virtual machine **at least 12GB** of (dynamic) harddrive space (CLion and wxWidgets need quite a lot of space).

### 1.1 Prepare OS Environment

#### Ubuntu 20.4
The OS should already have git installed. If not, you can use: 
`sudo apt-get install git`

Then use either `git clone git@gitlab.ethz.ch:vogelpa/software-engineering-group-project.git`(with SSH) or `git clone https://gitlab.ethz.ch/vogelpa/software-engineering-group-project.git` (with HTTPS) to fetch the repository.

Execute the following commands in a console:
1. `sudo apt-get update`
2. `sudo apt-get install build-essential` followed by `sudo reboot`
3. if on virtual machine : install guest-additions (https://askubuntu.com/questions/22743/how-do-i-install-guest-additions-in-a-virtualbox-vm) and then `sudo reboot`
4. `sudo snap install clion --classic` this installs the latest stable CLion version
5. `sudo apt-get install libwxgtk3.0-gtk3-dev` this installs wxWidgets (GUI library used in this project)

#### MacOS
Install [homebrew](https://brew.sh/) if it is not already installed. Then install git if it is not already in your OS with:
`brew install git`

Then use either `git clone git@gitlab.ethz.ch:vogelpa/software-engineering-group-project.git`(with SSH) or `git clone https://gitlab.ethz.ch/vogelpa/software-engineering-group-project.git` (with HTTPS) to fetch the repository.

Execute the following commands in a console:
1. `brew install --cask clion`
2. `brew install wxwidgets`


### 1.2 Compile Code
#### Ubuntu 20.4:
1. Open Clion
2. Click `File > Open...` and there select the **/sockpp** folder of this project
3. Click `Build > Build all in 'Debug'`
4. Wait until sockpp is compiled (from now on you never have to touch sockpp again ;))
5. Click `File > Open...` select the **/software-engineering-group-project** folder
6. Click `Build > Build all in 'Debug'`
7. Wait until everything is compiled.

#### MacOS:
1. Open Clion
2. Click `File > Open...` and there select the **/sockpp** folder of this project
3. Select the CMake file of this project, make a right mouse click and select `Load Project`.
4. Click `Build > Build all in 'Debug'`
5. Wait until sockpp is compiled (from now on you never have to touch sockpp again)
6. Click `File > Open...` select the **/software-engineering-group-project** folder
7. Select the CMake file of this project, make a right mouse click and select `Load Project`.
6. Click `Build > Build all in 'Debug'`
7. Wait until everything is compiled.

## 2. Run the Game 
### 2.1. Locally without Separate Server Console
Note: For this you need to have gnome-terminal installed. Simply run `sudo apt-get install gnome-terminal` if you do not have it installed already.

1. Open a console in the project folder, navigate into "cmake-build-debug" `cd cmake-build-debug`.
2. In new consoles run as many clients as you want players `./Skull-King`.
3. In one of the opened interfaces you need to create a server with the "Create Server" button.
4. All other players can just connect normally by inserting a player name and pressing "Connect".

### 2.2. Locally with Separate Server Console
If you do not want to install gnome-terminal use this to run the game.

1. Open a console in the project folder, navigate into "cmake-build-debug" `cd cmake-build-debug`.
2. Run `./Backend-Skull-King` in your console.
2. In new consoles run as many clients as you want players with the command `./Skull-King`
4. All players can just connect normally by inserting a player name and pressing "Connect".

### 2.3. Run the Game with an Online Server
1. For this you need to install ngrok, this is possible by running the command: `curl -s https://ngrok-agent.s3.amazonaws.com/ngrok.asc | sudo tee /etc/apt/trusted.gpg.d/ngrok.asc >/dev/null && echo "deb https://ngrok-agent.s3.amazonaws.com/ buster main" | sudo tee /etc/apt/sources.list.d/ngrok.list && sudo apt update && sudo apt-get install ngrok`.
2. Then create an ngrok account on [ngrok signup](https://dashboard.ngrok.com/signup).
3. Once you received a token install it with `ngrok config add-authtoken <token>` locally. The token you find here: [ngrok token](https://dashboard.ngrok.com/get-started/your-authtoken).
4. Then start a tunnel with the command ngrok `tcp <localport> --region eu`, the standard localport to use is 50505. Now it should display the name and port of the created connection.
5. Open a console in the project folder, navigate into "cmake-build-debug" `cd cmake-build-debug`.
6. Run `./Skull-King` like in a normal session.
7. Press "Create server". This will start a server in a separate gnome-terminal.
8. Insert the name of the ngrok server address (e.g.: '2.tcp.ngrok.eu.io') and the respective port number (e.g.: 16852) and press "Connect".
9. Tell your friends the server name and the port number, so they can simply join with their computer by entering their name after running `./Skull-King` in a console and pressing "Connect".

If you need further information about installing ngrok visit [ngrok install](https://ngrok.com/download).

## 3. Play the Game
### 3.1 Game Window
The game window is the main frame that will display all the panels. You can find the options "Scoreboard", "Rules" and "Quit" in the menu bar (Windows/Linux) respectively the title bar (macOS).

<img src="./pictures_readme/top_panel_mac.png" width=80% height=50%>


##### Scoreboard: 
During the game you can look at the scoreboard to see how you and your peers are doing by pressing on "Scoreboard" in the top panel.

<img src="./pictures_readme/scoreboard.png" width=20% height=20%>

##### Rules: 
To reread the rules or learn them before you play, you can have a look at them by pressing "Rules" in the top panel.

<img src="./pictures_readme/rules.png" width=40% height=20%>

##### Quit: 
If you want to quit the game you can press "Quit" in the top panel.

## 3.2 Game Panels
There are three different GUI panels that show up when playing the game:
### Connection Panel
When starting the game you are first in the connection panel. This is used to build a server, to start a game and to enter your name. How to make a server or start a game is better described in point 2 "Run the Game".

![connection panel](./pictures_readme/connection_panel_mac.png)

### Bid Panel
At the start of every round you are asked to place a bid, which represents the number of tricks you think you win. You can only select between zero and the number of the cards that are distributed in this round. While placing the bid you already see which cards you have and at which point in the first trick of this round you are able to play. This makes it easier to place a evaluated bid.

![bid panel](./pictures_readme/bid_panel_mac.png)

### Main Panel
During the actual round you are always in the main panel. Here you can see the other players, how many cards each player still has, the bid and the already won tricks of all players. Additionally you see all your cards and which players turn it is. If it is your turn you can simply click on the card you want to play. At all times you can see the wanted color in the background.

![main panel](./pictures_readme/main_panel_mac.png)

If you want to play a card that is not valid at the moment an error message pops up and you can select another card:

![wrong color](./pictures_readme/wrong_color_mac.png)



## 4. Leave the Game

If you want to leave the game while playing then press "Quit" in the menu/title bar or leave by closing the game window (pressing the red 'x' in the title bar). The other players will be able to continue playing.

## 5. Run the Unit Tests
1. CLion should automatically create a Google Test configuration SkullKing-tests which will run all tests. See [Google Test run/debug configuration﻿](https://www.jetbrains.com/help/clion/creating-google-test-run-debug-configuration-for-test.html#gtest-config) for more information.
2. From the list on the main toolbar, select the configuration SkullKing-tests.
3. Click ![run](https://resources.jetbrains.com/help/img/idea/2021.1/artwork.studio.icons.shell.toolbar.run.svg) or press `Shift+F10`.
   
You can run individual tests or test suites by opening the corresponding file in the **/unit-tests** folder and clicking ![run](https://resources.jetbrains.com/help/img/idea/2021.1/artwork.studio.icons.shell.toolbar.run.svg) next to the test method or class. For more information on testing in CLion read the [documentation](https://www.jetbrains.com/help/clion/performing-tests.html).



