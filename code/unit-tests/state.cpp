#include "gtest/gtest.h"
#include "../src/common/game_state/GameState.h"

// initialisation of the draw pile should result in empty pile
TEST(PileTest, InitEmptyDrawPile) {
    DrawPile p;
    EXPECT_TRUE(p.get_nof_cards() == 0);
    EXPECT_TRUE(p.is_empty());
}

TEST(PlayerTest, StartGameWithOnePlayer) {
    GameState game_state;
    Player a("A");
    std::string error_string;
    EXPECT_TRUE(game_state.add_player(&a, error_string));
    EXPECT_TRUE(error_string.empty()); //adding a player should be possible
    EXPECT_TRUE(game_state.is_player_in_game(&a));

    game_state.start_game(error_string);
    EXPECT_FALSE(error_string.empty()); // starting a game with one player shouldn't be possible
    EXPECT_FALSE(game_state.is_started());
}

TEST(PlayerTest, StartGameWithThreePlayers) {
    GameState game_state;
    Player a("A");
    Player b("B");
    Player c("C");
    std::string error_string;

    game_state.add_player(&a, error_string);
    game_state.add_player(&b, error_string);
    game_state.add_player(&c, error_string);
    EXPECT_TRUE(game_state.start_game(error_string));
    EXPECT_TRUE(game_state.is_started());
    EXPECT_TRUE(a.get_nof_cards()==1);
    EXPECT_TRUE(b.get_nof_cards()==1);
    EXPECT_TRUE(c.get_nof_cards()==1);
    EXPECT_FALSE(game_state.are_bids_placed());
}

TEST(PlayerTest, AddingTooManyPlayers){
    GameState game_state;
    Player a("A"); Player b("B"); Player c("C");
    Player d("D"); Player e("E"); Player f("F");
    Player g("G"); Player h("H");
    std::string error_string;

    game_state.add_player(&a, error_string); game_state.add_player(&b, error_string);
    game_state.add_player(&c, error_string); game_state.add_player(&d, error_string);

    game_state.add_player(&a, error_string); //try adding same player twice
    EXPECT_EQ(error_string, "Could not join game, because this Player is already part of this game.");

    game_state.add_player(&e, error_string); game_state.add_player(&f, error_string);

    game_state.add_player(&g, error_string); //try adding more than max players
    EXPECT_EQ(error_string, "Could not join game, because the max number of players is already reached.");
}


//setting up a playedpile should result in an empty pile
TEST(PileTest, InitEmptyPlayedPile) {
    PlayedPile played_pile;
    std::string error_string;
    EXPECT_TRUE(played_pile.get_currentType() == 0); //need to add case
    EXPECT_TRUE(played_pile.get_cards().empty()); //need to add function
}

//distribute x cards
TEST(CardsTest, DistributeCards) {
    DrawPile draw_pile;
    int nof_cards = 5;
    Player a("A");
    std::string error_string;

    draw_pile.setup_game(error_string);
    EXPECT_TRUE(draw_pile.distributeCards(&a, nof_cards, error_string));
    EXPECT_TRUE(a.get_nof_cards() == nof_cards);
}

TEST(BidTest, PlaceBidGameNotStarted) {
    GameState game_state;
    Player a("A");
    std::string error_string;

    game_state.add_player(&a, error_string);

    EXPECT_FALSE(game_state.placeBid(&a, 0, error_string)); //game hasn't started
}

TEST(BidTest, PlaceBidGameStarted) {
    GameState game_state;
    Player a("A");
    Player b("B");
    std::string error_string;

    game_state.add_player(&a, error_string);
    game_state.add_player(&b, error_string);
    game_state.start_game(error_string);

    EXPECT_TRUE(game_state.placeBid(&a, 1, error_string));
    EXPECT_EQ(a.get_bid(), 1);
}

TEST(LeaveTest, RemovePlayerInLobby) {
    GameState game_state;
    Player* a = new Player("A");
    Player b("B");

    std::string error_string;
    game_state.add_player(a, error_string);
    game_state.add_player(&b, error_string);

    EXPECT_TRUE(game_state.remove_player(a, error_string)); //successfully removes player from game
    EXPECT_FALSE(game_state.is_player_in_game(a)); //double-check
}

TEST(LeaveTest, RemovePlayerWhileBidding) {
    GameState game_state;
    Player* a = new Player("A");
    Player b("B");
    Player c("C");

    std::string error_string;
    game_state.add_player(a, error_string);
    game_state.add_player(&b, error_string);
    game_state.add_player(&c, error_string);

    EXPECT_TRUE(game_state.remove_player(a, error_string)); //successfully removes player from game
    EXPECT_FALSE(game_state.is_player_in_game(a)); //double-check
}

//play a card
TEST (CardsTest, PlayCard) {
    GameState game_state;
    Player a("A");
    Player b("B");
    std::string error_string;

    game_state.add_player(&a, error_string);
    game_state.add_player(&b, error_string);
    game_state.start_game(error_string);

    game_state.placeBid(&a, 0, error_string);
    game_state.placeBid(&b, 0, error_string);

    std::string card_id = a.get_hand()->get_cards().at(0)->get_id();
    EXPECT_TRUE(game_state.play_card(&a, card_id, error_string));
    EXPECT_EQ(a.get_nof_cards(), 0);
}

TEST(CardsTest, PlayCardNotYourTurn) {
    GameState game_state;
    Player a("A");
    Player b("B");
    std::string error_string;

    game_state.add_player(&a, error_string);
    game_state.add_player(&b, error_string);
    game_state.start_game(error_string);

    game_state.placeBid(&a, 0, error_string);
    game_state.placeBid(&b, 0, error_string);

    std::string card_id = b.get_hand()->get_cards().at(0)->get_id();
    EXPECT_FALSE(game_state.play_card(&b, card_id, error_string));
    EXPECT_EQ(error_string, "It's not your turn yet.");
}

TEST(CardsTest, PlayCardBidsNotPlaced) {
    GameState game_state;
    Player a("A");
    Player b("B");
    std::string error_string;

    game_state.add_player(&a, error_string);
    game_state.add_player(&b, error_string);
    game_state.start_game(error_string);

    game_state.placeBid(&a, 0, error_string);

    std::string card_id = a.get_hand()->get_cards().at(0)->get_id();
    EXPECT_FALSE(game_state.play_card(&a, card_id, error_string));
    EXPECT_EQ(error_string, "You must wait until all players have placed their bid.");
}

TEST(PileTest, DetermineWinnerColor){
    std::string err;
    std::string id = "pile";
    serializable_value<int>* current_type = new serializable_value<int>(2);
    Card* r12 = new Card(12, 2);
    Card* r3 = new Card(3, 2);
    Card* r13 = new Card(13, 2);
    std::vector<Card*> pile = {r12, r3, r13};

    PlayedPile played_pile(id,  pile, current_type);
    std::pair<int,int> ret = played_pile.determine_winner_of_trick();
    EXPECT_EQ(ret.first, 2);
    EXPECT_EQ(ret.second, 0);
}

TEST(PileTest, DetermineWinnerColors){
    std::string err;
    std::string id = "pile";
    serializable_value<int>* current_type = new serializable_value<int>(2);
    Card* r6 = new Card(6, 2);
    Card* b10 = new Card(10, 1);
    Card* y13 = new Card(13, 3);
    std::vector<Card*> pile = {r6, b10, y13};

    PlayedPile played_pile(id,  pile, current_type);
    std::pair<int,int> ret = played_pile.determine_winner_of_trick();
    EXPECT_EQ(ret.first, 0);
    EXPECT_EQ(ret.second, 0);
}

TEST(PileTest, DetermineWinnerColorBlack){
    std::string err;
    std::string id = "pile";
    serializable_value<int>* current_type = new serializable_value<int>(2);
    Card* r6 = new Card(6, 2);
    Card* b10 = new Card(10, 1);
    Card* bl1 = new Card(1, 4);
    std::vector<Card*> pile = {r6, b10, bl1};

    PlayedPile played_pile(id,  pile, current_type);
    std::pair<int,int> ret = played_pile.determine_winner_of_trick();
    EXPECT_EQ(ret.first, 2);
    EXPECT_EQ(ret.second, 0);
}

TEST(PileTest, DetermineWinnerPirate){
    std::string err;
    std::string id = "pile";
    serializable_value<int>* current_type = new serializable_value<int>(2);
    Card* r12 = new Card(12, 2);
    Card* bl3 = new Card(3, 4);
    Card* pirate = new Card(15, 0);
    std::vector<Card*> pile = {r12, bl3, pirate};

    PlayedPile played_pile(id,  pile, current_type);
    std::pair<int,int> ret = played_pile.determine_winner_of_trick();
    EXPECT_EQ(ret.first, 2);
    EXPECT_EQ(ret.second, 0);
}

TEST(PileTest, DetermineWinnerPirateMermaid){
    std::string err;
    std::string id = "pile";
    serializable_value<int>* current_type = new serializable_value<int>(2);
    Card* mermaid = new Card(14, 0);
    Card* pirate1 = new Card(15, 0);
    Card* pirate2 = new Card(15, 0);
    std::vector<Card*> pile = {mermaid, pirate1, pirate2};

    PlayedPile played_pile(id,  pile, current_type);
    std::pair<int,int> ret = played_pile.determine_winner_of_trick();
    EXPECT_EQ(ret.first, 1);
    EXPECT_EQ(ret.second, 0);
}

TEST(PileTest, DetermineWinnerSkullkingMermaid){
    std::string err;
    std::string id = "pile";
    serializable_value<int>* current_type = new serializable_value<int>(2);
    Card* mermaid = new Card(14, 0);
    Card* skullking = new Card(16, 0);
    Card* r7 = new Card(7, 3);
    std::vector<Card*> pile = {mermaid, skullking, r7};

    PlayedPile played_pile(id,  pile, current_type);
    std::pair<int,int> ret = played_pile.determine_winner_of_trick();
    EXPECT_EQ(ret.first, 0);
    EXPECT_EQ(ret.second, 50);
}

TEST(PileTest, DetermineWinnerSkullkingTwoMermaids){
    std::string err;
    std::string id = "pile";
    serializable_value<int>* current_type = new serializable_value<int>(2);
    Card* mermaid1 = new Card(14, 0);
    Card* mermaid2 = new Card(14, 0);
    Card* skullking = new Card(16, 0);
    Card* pirate = new Card(7, 3);
    std::vector<Card*> pile = {pirate, mermaid1, skullking, mermaid2};

    PlayedPile played_pile(id,  pile, current_type);
    std::pair<int,int> ret = played_pile.determine_winner_of_trick();
    EXPECT_EQ(ret.first, 1);
    EXPECT_EQ(ret.second, 50);
}

TEST(PileTest, DetermineWinnerOnlyFlags){
    std::string err;
    std::string id = "pile";
    serializable_value<int>* current_type = new serializable_value<int>(2);
    Card* flag1 = new Card(0, 0);
    Card* flag2 = new Card(0, 0);
    Card* flag3 = new Card(0, 0);
    std::vector<Card*> pile = {flag1, flag2, flag3};

    PlayedPile played_pile(id,  pile, current_type);
    std::pair<int,int> ret = played_pile.determine_winner_of_trick();
    EXPECT_EQ(ret.first, 0);
    EXPECT_EQ(ret.second, 0);
}

TEST(PileTest, DetermineWinnerMaximumBonus){
    std::string err;
    std::string id = "pile";
    serializable_value<int>* current_type = new serializable_value<int>(2);
    Card* pirate1 = new Card(15, 0);
    Card* pirate2 = new Card(15, 0);
    Card* pirate3 = new Card(15, 0);
    Card* pirate4 = new Card(15, 0);
    Card* pirate5 = new Card(15, 0);
    Card* skullking = new Card(16, 0);

    std::vector<Card*> pile = {pirate1, skullking, pirate2, pirate3, pirate4, pirate5};

    PlayedPile played_pile(id,  pile, current_type);
    std::pair<int,int> ret = played_pile.determine_winner_of_trick();
    EXPECT_EQ(ret.first, 1);
    EXPECT_EQ(ret.second, 150);
}

TEST(LeaveTest, PlayerLeavesLastToPlaceBid){
    GameState game_state;
    Player a("A");
    Player b("B");
    Player* c = new Player("C");

    std::string error_string;
    game_state.add_player(&a, error_string);
    game_state.add_player(&b, error_string);
    game_state.add_player(c, error_string);
    game_state.start_game(error_string);

    game_state.placeBid(&a, 0, error_string);
    game_state.placeBid(&b, 0, error_string);

    EXPECT_TRUE(game_state.remove_player(c, error_string));
    EXPECT_FALSE(game_state.is_player_in_game(c));

    std::string card_id = a.get_hand()->get_cards().at(0)->get_id();
    EXPECT_TRUE(game_state.play_card(&a, card_id, error_string));
}

TEST(LeaveTest, PlayerLeavesNoCardPlayed){
    GameState game_state;
    Player a("A");
    Player b("B");
    Player* c = new Player("C");

    std::string error_string;
    game_state.add_player(&a, error_string);
    game_state.add_player(&b, error_string);
    game_state.add_player(c, error_string);
    game_state.start_game(error_string);

    game_state.placeBid(&a, 0, error_string);
    game_state.placeBid(&b, 0, error_string);
    game_state.placeBid(c, 0, error_string);

    std::string card_id = a.get_hand()->get_cards().at(0)->get_id();
    game_state.play_card(&a, card_id, error_string);
    //player hasn't played a card yet so he can be removed immediately
    EXPECT_TRUE(game_state.remove_player(c, error_string));
    EXPECT_FALSE(game_state.is_player_in_game(c));
}

TEST(LeaveTest, PlayerLeavesCardPlayed){
    GameState game_state;
    Player* a = new Player("A");
    Player b("B");
    Player c("C");

    std::string error_string;
    game_state.add_player(a, error_string);
    game_state.add_player(&b, error_string);
    game_state.add_player(&c, error_string);
    game_state.start_game(error_string);

    game_state.placeBid(a, 0, error_string);
    game_state.placeBid(&b, 0, error_string);
    game_state.placeBid(&c, 0, error_string);

    std::string card_id = a->get_hand()->get_cards().at(0)->get_id();
    game_state.play_card(a, card_id, error_string);
    EXPECT_TRUE(game_state.remove_player(a, error_string));
    //if the player has played a card already he remains in the game until the end of the trick
    EXPECT_TRUE(game_state.is_player_in_game(a));

    card_id = b.get_hand()->get_cards().at(0)->get_id();
    game_state.play_card(&b, card_id, error_string);
    card_id = c.get_hand()->get_cards().at(0)->get_id();
    game_state.play_card(&c, card_id, error_string);
    game_state.wrap_up_trick(error_string);
    EXPECT_FALSE(game_state.is_player_in_game(a));
    //in the next round the 0th player should start (0th player was a and is now b is 0th player)
    EXPECT_EQ(game_state.get_current_player(), game_state.get_players()[0]);
}

TEST(LeaveTest, PlayerLeavesTooFewPlayers){
    GameState game_state;
    Player a("A");
    Player* b = new Player("B");

    std::string error_string;
    game_state.add_player(&a, error_string);
    game_state.add_player(b, error_string);
    game_state.start_game(error_string);

    EXPECT_TRUE(game_state.remove_player(b, error_string));
    EXPECT_FALSE(game_state.is_player_in_game(b));

    EXPECT_TRUE(game_state.is_finished());
}

TEST(LeaveTest, SeveralPlayersLeave){
    GameState game_state;
    Player* a = new Player("A");
    Player* b = new Player("B");
    Player* c = new Player("C");
    Player* d = new Player("D");
    Player* e = new Player("E");
    Player* f = new Player("F");

    std::string error_string;

    game_state.add_player(a, error_string);
    game_state.add_player(b, error_string);
    game_state.add_player(c, error_string);
    game_state.add_player(d, error_string);
    game_state.add_player(e, error_string);
    game_state.add_player(f, error_string);

    game_state.start_game(error_string);

    game_state.placeBid(a, 0, error_string);
    game_state.placeBid(b, 0, error_string);
    game_state.placeBid(c, 0, error_string);
    game_state.placeBid(d, 0, error_string);
    game_state.placeBid(e, 0, error_string);
    game_state.placeBid(f, 0, error_string);

    std::string card_id = a->get_hand()->get_cards().at(0)->get_id();
    game_state.play_card(a, card_id, error_string);
    card_id = b->get_hand()->get_cards().at(0)->get_id();
    game_state.play_card(b, card_id, error_string);
    card_id = c->get_hand()->get_cards().at(0)->get_id();
    game_state.play_card(c, card_id, error_string);

    game_state.remove_player(c, error_string);

    card_id = d->get_hand()->get_cards().at(0)->get_id();
    game_state.play_card(d, card_id, error_string);

    game_state.remove_player(a, error_string);

    card_id = e->get_hand()->get_cards().at(0)->get_id();
    game_state.play_card(e, card_id, error_string);

    EXPECT_TRUE(game_state.is_player_in_game(a));
    EXPECT_TRUE(game_state.is_player_in_game(c));

    //its f's turn, but he leaves, wrap_up_trick & wrap_up_round should be called
    game_state.remove_player(f, error_string);
    EXPECT_EQ(game_state.get_round_counter(), 2);
    EXPECT_FALSE(game_state.is_player_in_game(a));
    EXPECT_FALSE(game_state.is_player_in_game(c));
    EXPECT_FALSE(game_state.is_player_in_game(f));

    //Check that the game goes on (its b's turn)
    game_state.placeBid(b, 0, error_string);
    game_state.placeBid(d, 0, error_string);
    game_state.placeBid(e, 0, error_string);

    card_id = b->get_hand()->get_cards().at(0)->get_id();
    EXPECT_TRUE(game_state.play_card(b, card_id, error_string));

    delete b;
    delete d;
    delete e;
}