// 13.4. ready for testing

// The game_instance class is a wrapper around the game_state of an active instance of the game.
// This class contains functions to modify the contained game_state.

#ifndef SkullKing_GAME_H
#define SkullKing_GAME_H

#include <vector>
#include <string>
#include <mutex>

#include "../common/game_state/player/Player.h"
#include "../common/game_state/GameState.h"

class game_instance {

private:
    GameState* _game_state;
    bool is_player_allowed_to_play(Player* player);
    inline static std::mutex modification_lock; //TODO: why is this mutex static?

public:
    game_instance();
    ~game_instance() {
        if (_game_state != nullptr) {
            delete _game_state;
        }
        _game_state = nullptr;
    }
    std::string get_id();

    GameState* get_game_state();

    bool is_full();
    bool is_started();
    bool is_finished();

    // game update functions
    bool start_game(Player* player, std::string& err);
    bool try_add_player(Player* new_player, std::string& err);
    bool try_remove_player(Player* player, std::string& err);
    bool play_card(Player* player, const std::string& card_id, std::string& err);
    bool placeBid(Player* player, int bid, std::string& err);

};


#endif //SkullKing_GAME_H