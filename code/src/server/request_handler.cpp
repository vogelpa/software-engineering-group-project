#ifndef SkullKing_REQUEST_HANDLER_CPP
#define SkullKing_REQUEST_HANDLER_CPP

#include "request_handler.h"

#include "player_manager.h"
#include "game_instance_manager.h"
#include "game_instance.h"

#include "../common/network/requests/join_game_request.h"
#include "../common/network/requests/play_card_request.h"
#include "../common/network/requests/place_bid_request.h"

request_response* request_handler::handle_request(const client_request* const req) {

    // Prepare variables that are used by every request type
    Player* player;
    std::string err;
    game_instance* game_instance_ptr = nullptr;

    // Get common properties of requests
    RequestType type = req->get_type();
    std::string req_id = req->get_req_id();
    std::string game_id = req->get_game_id();
    std::string player_id = req->get_player_id();


    // Switch behavior according to request type
    switch(type) {

        // ##################### JOIN GAME #####################  //
        case RequestType::join_game: {
            std::string player_name = ((join_game_request *) req)->get_player_name();

            // Create new Player or get existing one with that name
            player_manager::add_or_get_player(player_name, player_id, player);

            if (game_id.empty()) {
                // join any game
                if (game_instance_manager::try_add_player_to_any_game(player, game_instance_ptr, err)) {
                    // game_instance_ptr got updated to the joined game

                    // return response with full game_state attached
                    rapidjson::Document* json2 = new rapidjson::Document();
                    json2->SetObject();
                    game_instance_ptr->get_game_state()->write_into_json(*json2, json2->GetAllocator(), player);
                    
                    return new request_response(game_instance_ptr->get_id(), req_id, true,
                                                json2, err);
                } else {
                    // failed to find game to join
                    return new request_response("", req_id, false, nullptr, err);
                }
            } else {
                // join a specific game denoted by req->get_game_id()
                if (game_instance_manager::try_get_game_instance(game_id, game_instance_ptr)) {
                    if (game_instance_manager::try_add_player(player, game_instance_ptr, err)) {
                        // return response with full game_state attached
                        rapidjson::Document* json2 = new rapidjson::Document();
                        json2->SetObject();
                        game_instance_ptr->get_game_state()->write_into_json(*json2, json2->GetAllocator(), player);
                    
                        return new request_response(game_instance_ptr->get_id(), req_id, true,
                                                json2, err);
                    } else {
                        // failed to join requested game
                        return new request_response("", req_id, false, nullptr, err);
                    }
                } else {
                    // failed to find requested game
                    return new request_response("", req_id, false, nullptr, "Requested game could not be found.");
                }
            }
        }


        // ##################### START GAME ##################### //
        case RequestType::start_game: {
            if (game_instance_manager::try_get_player_and_game_instance(player_id, player, game_instance_ptr, err)) {
                if (game_instance_ptr->start_game(player, err)) {
                    rapidjson::Document* json2 = new rapidjson::Document();
                    json2->SetObject();
                    game_instance_ptr->get_game_state()->write_into_json(*json2, json2->GetAllocator(), player);
                    
                    return new request_response(game_instance_ptr->get_id(), req_id, true,
                                                json2, err);
                }
            }
            return new request_response("", req_id, false, nullptr, err);
        }


        // ##################### PLAY CARD ##################### //
        case RequestType::play_card: {
            if (game_instance_manager::try_get_player_and_game_instance(player_id, player, game_instance_ptr, err)) {
                Card *drawn_card;
                std::string card_id = ((play_card_request *) req)->get_card_id();
                if (game_instance_ptr->play_card(player, card_id, err)) {
                    rapidjson::Document* json2 = new rapidjson::Document();
                    json2->SetObject();
                    game_instance_ptr->get_game_state()->write_into_json(*json2, json2->GetAllocator(), player);
                    
                    return new request_response(game_instance_ptr->get_id(), req_id, true,
                                                json2, err);
                }
            }
            return new request_response("", req_id, false, nullptr, err);
        }


        // // ##################### PLACE BID ##################### //
        case RequestType::place_bid: {
            if (game_instance_manager::try_get_player_and_game_instance(player_id, player, game_instance_ptr, err)) {
                int bid = ((place_bid_request *) req)->get_bid_amount();
                if (game_instance_ptr->placeBid(player, bid, err)) {
                    rapidjson::Document* json2 = new rapidjson::Document();
                    json2->SetObject();
                    game_instance_ptr->get_game_state()->write_into_json(*json2, json2->GetAllocator(), player);
                    
                    return new request_response(game_instance_ptr->get_id(), req_id, true,
                                                json2, err);
                }
            }
            return new request_response("", req_id, false, nullptr, err);
        }


        // // ##################### LEAVE GAME ##################### //
        case RequestType::leave_game:{
            if (game_instance_manager::try_get_player_and_game_instance(player_id, player, game_instance_ptr, err)) {
                //remove the player from the game
                game_instance_ptr->try_remove_player(player, err);
                //remove the player from the players_lut, otherwise we trash memory (only relevant if server stays online for a very long time)
                player_manager::remove_player(player_id, player);
            }
            return new request_response("", req_id, true, nullptr, "");
        }
        

        // ##################### UNKNOWN REQUEST ##################### //
        default:
            return new request_response("", req_id, false, nullptr, "Unknown RequestType " + type);
    }
}

#endif //SkullKing_REQUEST_HANDLER_CPP