// 13.4. ready for testing

// The game_instance class is a wrapper around the game_state of an active instance of the game.
// This class contains functions to modify the contained game_state.

#include "game_instance.h"

#include "server_network_manager.h"
#include "../common/network/responses/full_state_response.h"
#include <chrono>
#include <thread>

game_instance::game_instance() {
    _game_state = new GameState();
}

GameState *game_instance::get_game_state() {
    return _game_state;
}

std::string game_instance::get_id() {
    return _game_state->get_id();
}

bool game_instance::is_player_allowed_to_play(Player *player) {
    return _game_state->is_allowed_to_play_now(player);
}

bool game_instance::is_full() {
    return _game_state->is_full();
}

bool game_instance::is_started() {
    return _game_state->is_started();
}

bool game_instance::is_finished() {
    return _game_state->is_finished();
}

bool game_instance::play_card(Player *player, const std::string& card_id, std::string& err) {
    modification_lock.lock();
    if (_game_state->play_card(player, card_id, err)) {
       for(auto &allplayer: _game_state->get_players()){
                full_state_response state_update_msg = full_state_response(this->get_id(), *_game_state, allplayer);
                server_network_manager::send_message_to_player(state_update_msg,  allplayer);
            }
        //if the last player of the trick has played a card, wrap up the trick
        if (_game_state->is_trick_finished()){
            _game_state->wrap_up_trick(err);
            //show last card played for 3 seconds before sending new gamestate
            std::this_thread::sleep_for(std::chrono::seconds(3));
            for(auto &allplayer: _game_state->get_players()){
                if(allplayer!= player) {
                    full_state_response state_update_msg = full_state_response(this->get_id(), *_game_state, allplayer);
                    server_network_manager::send_message_to_player(state_update_msg, allplayer);
                }
            }
        }
        //need this for ngrok
        std::this_thread::sleep_for(std::chrono::milliseconds (200));
        modification_lock.unlock();
        return true;
    }
    //need this for ngrok
    std::this_thread::sleep_for(std::chrono::milliseconds (200));
    modification_lock.unlock();
    return false;
}

bool game_instance::start_game(Player* player, std::string &err) {
    modification_lock.lock();
    if (_game_state->start_game(err)) {
        for (auto &allplayer: _game_state->get_players()){
            if (allplayer != player){
                full_state_response state_update_msg = full_state_response(this->get_id(), *_game_state, allplayer);
                server_network_manager::send_message_to_player(state_update_msg,  allplayer);
            }
        }
        modification_lock.unlock();
        return true;
    }
    modification_lock.unlock();
    return false;
}

bool game_instance::placeBid(Player* player, int bids, std::string &err) {
    modification_lock.lock();
    //send update only after all bids are placed
    if (_game_state->placeBid(player, bids, err)) {
        if(_game_state->are_bids_placed()){ //function that checks if all bets are placed
        for(auto &allplayer: _game_state->get_players()){
                if(allplayer!= player){
                    full_state_response state_update_msg = full_state_response(this->get_id(), *_game_state, allplayer);
                    server_network_manager::send_message_to_player(state_update_msg,  allplayer);
                }
            }
        }
        //if not all bets are placed we don't send the update to all players yet, but place_bid has been successful
        modification_lock.unlock();
        return true;
    }
    modification_lock.unlock();
    return false;
}

bool game_instance::try_remove_player(Player *player, std::string &err) {
    modification_lock.lock();
    //if players leave at the end of the game, we don't send updates to the other plyers anymore
    //starting
    if (!_game_state->is_finished() && _game_state->remove_player(player, err)) {
        for(auto &allplayer: _game_state->get_players()){
                if(allplayer!= player){
                full_state_response state_update_msg = full_state_response(this->get_id(), *_game_state, allplayer);
                server_network_manager::send_message_to_player(state_update_msg,  allplayer);
                }
        }
        modification_lock.unlock();
        return true;
    }
    modification_lock.unlock();
    return false;
}

bool game_instance::try_add_player(Player *new_player, std::string &err) {
    modification_lock.lock();
    if (_game_state->add_player(new_player, err)) {
        new_player->set_game_id(get_id());
        // send state update to all other players
        for(auto &allplayer: _game_state->get_players()){
                if(allplayer!= new_player){
                full_state_response state_update_msg = full_state_response(this->get_id(), *_game_state, allplayer);
                server_network_manager::send_message_to_player(state_update_msg,  allplayer);
                }
            }
        modification_lock.unlock();
        return true;
    }
    modification_lock.unlock();
    return false;
}