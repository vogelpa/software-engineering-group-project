//
// Created by p-b99 on 11/05/2022.
//

#ifndef SKULLKING_RULESWINDOW_H
#define SKULLKING_RULESWINDOW_H

#endif //SKULLKING_RULESWINDOW_H

#include <wx/wx.h>
#include <wx/image.h>

class RulesWindow: public wxDialog {
public:
    RulesWindow(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos=wxDefaultPosition, const wxSize& size=wxDefaultSize, long style=wxDEFAULT_DIALOG_STYLE);
private:
protected:
    wxScrolledWindow* panel_1;
};