#include "ScoreBoard.h"

//constructor
ScoreBoard::ScoreBoard(wxFrame* frame, int numberOfPlayers): wxGrid(frame,wxID_ANY){

    //private variables
    _col = numberOfPlayers + 1;
    _row = 11;
    _round = 1;
    CreateGrid( _row, _col);

    //all to avoid any interaction between user and table
    EnableEditing(false);
    DisableCellEditControl();
    SetCellHighlightPenWidth(0);
    SetCellHighlightROPenWidth(0);
    DisableDragColMove();
    DisableDragColSize();
    DisableDragGridSize();
    DisableDragRowSize();
    ClearSelection();
    HideCellEditControl ();
    HideColLabels();
    HideRowLabels ();

    //layout
    for(int i = 0; i < _row; i++){
        SetRowSize(i,50);
    }
    
    for(int j = 0; j < _col; j++){
        SetColSize(j,80);
    }   
    
    SetDefaultCellAlignment(wxALIGN_CENTRE,wxALIGN_CENTRE);
    SetGridLineColour("BLACK");
    SetCellBackgroundColour(0,0,"DIM GREY");
}

//fill the next line of the scoreboard
void ScoreBoard::fill(const std::vector<std::string>& id, std::vector<std::string>names, std::vector<int> points){

    wxColour bg;

    //set numbering and names
    if(GetCellValue(0,1).IsEmpty()){
        bg.Set("DARK GREY");
        for(int i = 1; i < _col; i++){
            if(names[i-1].size()>=8){
                SetCellValue(0,i,names[i-1].substr(0,6)+"...");
            }
            else
                SetCellValue(0,i,names[i-1]);
            SetCellBackgroundColour(0,i,bg);
        }
        for(int j = 1; j <_row; j++){
            SetCellValue(j,0,std::to_string(j));
            SetCellBackgroundColour(j,0,bg);
        }
    }

    while(!GetCellValue(_round,1).IsEmpty()){
        _round++;
    }

    //calculate values from previous values
    for(int i = 0; i < points.size(); i++){
        int previous=0;
        if(_round>1)
            previous= wxAtoi(GetCellValue(_round-1,i+1));

        if(points[i]==0){
            SetCellValue(_round,i+1,"X");
            bg.Set("MAROON");
            SetCellBackgroundColour(0,i+1,bg);
        }
        else
            SetCellValue(_round,i+1,std::to_string(previous+points[i]));

    }
}

//Window to show the scoreboard
//constructor
ScoreBoardFrame::ScoreBoardFrame(const wxString &title,const wxPoint &pos, const wxSize &size, long style, wxFrame * parent) :wxFrame(parent, wxID_ANY, title, pos,size, style){
    wxStaticText *TempBox = new wxStaticText(this, wxID_ANY, _T( "No scores yet" ), wxPoint( 50, 20 ), wxSize( 150,40 ), wxTE_CENTRE|wxBORDER_NONE);
    
    //adjust the size of the window
    wxBoxSizer* szr =new wxBoxSizer(wxVERTICAL);
    szr->AddStretchSpacer();
    szr->Add(TempBox, wxSizerFlags().Expand());
    szr->AddStretchSpacer();
    this->SetSizer(szr);  
}

//new standard to call events instead of binding
BEGIN_EVENT_TABLE(ScoreBoardFrame, wxFrame)
    EVT_CLOSE(ScoreBoardFrame::OnClose)
END_EVENT_TABLE()

//overwrite the cross on the top left corner
//now it doesn't destroy the window but just doesn't show it
//the window gets destroyed when the main window gets closed.
//this happens because the main window is the scoreboardframes parent
void ScoreBoardFrame::OnClose(wxCloseEvent& event) {
    Show(false);
}