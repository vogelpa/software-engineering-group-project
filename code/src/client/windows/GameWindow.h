#ifndef SkullKingUI_GAMEWINDOW_H
#define SkullKingUI_GAMEWINDOW_H

#include "../panels/ConnectionPanel.h"
#include <vector>
#include "ScoreBoard.h"

class GameWindow : public wxFrame
{
public:
    GameWindow(const wxString& title, const wxPoint& pos, const wxSize& size);

    void showPanel(wxPanel* panel);
    void setStatus(const std::string& message);
    void showMenubar();
    void updateBackground(wxColour color);

    void updateScoreboard(std::vector<std::string> player_names, std::vector<std::string> player_ids,
                          std::vector<int> scores);
    void OnClose(wxCloseEvent& event);
    void showScoarboard();

private:
    wxBoxSizer* _mainLayout;
    wxStatusBar* _statusBar;

    wxPanel* _currentPanel;
    ScoreBoardFrame* _scoreBoardWindow;
    ScoreBoard * _table;
    wxMenuBar *_menubar;
    wxMenu *_scoreboardMenu;
    wxMenuItem *_scoreboardItem;
    wxMenu *_rulesMenu;
    wxMenuItem *_rulesItem;
    wxMenu *_quitMenu;
    wxMenuItem *_quitItem;

    std::vector<std::vector<int>> _scores = {};
    std::vector<std::string> _player_names = {};
    std::vector<std::string> _player_ids = {};

    void scoreboardEventHandler(wxCommandEvent &event);
    void rulesEventHandler(wxCommandEvent &event);
    void quitEventHandler(wxCommandEvent &event);


};


#endif //SkullKingUI_GAMEWINDOW_H
