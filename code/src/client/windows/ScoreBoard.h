//
// Created by p-b99 on 11/05/2022.
//

#ifndef SCOREBOARD_H
#define SCOREBOARD_H


#include <vector>
#include <string>
#include <wx/wxprec.h>
#include <wx/wx.h>
#include <wx/grid.h>

class ScoreBoard:public wxGrid{
public:
    ScoreBoard(wxFrame* frame,int numberOfPlayers);
    void fill(const std::vector<std::string>& id, std::vector<std::string>names, std::vector<int> points);
private:
    int _col=0;
    int _row=0;
    int _round=1;
};

class ScoreBoardFrame : public wxFrame{
    public:
        ScoreBoardFrame(const wxString &titel,const wxPoint &pos, const wxSize &size, long style,wxFrame * parent);
        DECLARE_EVENT_TABLE()
        void OnClose(wxCloseEvent& event);
    private: 
};

#endif //SCOREBOARD_H