#include "GameWindow.h"
#include "../GameController.h"
#include "RulesWindow.h"
#include <wx/image.h>
#include <wx/grid.h>
#include "ScoreBoard.h"

GameWindow::GameWindow(const wxString &title, const wxPoint &pos, const wxSize &size) : wxFrame(nullptr, wxID_ANY,
                                                                                                title, pos, size) {
    // Set window size
    this->SetSize(wxSize(1080, 870));

    //To close Window
    this->Connect(wxEVT_CLOSE_WINDOW, wxCloseEventHandler(GameWindow::OnClose), NULL, this);

    // Set icon
    auto icon = new wxIcon("../assets/icon.xpm");
    SetIcon(*icon);

    // Add menu bar for scoreboard and rules
    _menubar = new wxMenuBar();
    _scoreboardMenu = new wxMenu();
    _scoreboardItem = _scoreboardMenu->Append(01, wxT("Scoreboard"), wxT("check complete scoreboard"));
    Bind(wxEVT_MENU, &GameWindow::scoreboardEventHandler, this, 01);
    _menubar->Append(_scoreboardMenu, wxT("Scoreboard"));
    _rulesMenu = new wxMenu();
    _rulesItem = _rulesMenu->Append(11, wxT("Rules"), wxT("check rulebook"));
    Bind(wxEVT_MENU, &GameWindow::rulesEventHandler, this, 11);
    _menubar->Append(_rulesMenu, wxT("Rules"));
    _quitMenu = new wxMenu();
    _quitItem = _quitMenu->Append(21, wxT("Quit"), wxEmptyString);
    Bind(wxEVT_MENU, &GameWindow::quitEventHandler, this, 21);
    _menubar->Append(_quitMenu, wxT("Quit"));
    SetMenuBar(nullptr);

    // Set up layout that will contain and center all content
    this->_mainLayout = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer *outerLayout = new wxBoxSizer(wxHORIZONTAL);
    outerLayout->Add(this->_mainLayout, 1, wxCENTER);
    this->SetSizerAndFit(outerLayout);
    this->_currentPanel = nullptr;

    // Set up status bar
    this->_statusBar = this->CreateStatusBar(1);

    // Set up frame for scoreboard
    _scoreBoardWindow =new ScoreBoardFrame("Scoreboard", wxDefaultPosition, wxDefaultSize,wxMINIMIZE_BOX | wxMAXIMIZE_BOX | wxSYSTEM_MENU
                       | wxCAPTION | wxCLOSE_BOX | wxCLIP_CHILDREN,this);

    

    // Set background
    wxColor lightGrey = wxColor(220, 220, 220);
    this->SetBackgroundColour(lightGrey);
    this->SetForegroundColour(wxColour(0, 0, 0));

    // Set the minimum size of the window. The user won't be able to resize the window to a size smaller than this
    this->SetMinSize(wxSize(1080, 870));
}

void GameWindow::showMenubar() {
    if (this->GetMenuBar() != this->_menubar) {
        this->SetMenuBar(_menubar);
    }
}

void GameWindow::scoreboardEventHandler(wxCommandEvent &event)  // wxGlade: MyFrame.<event_handler>
{
    _scoreBoardWindow->Show(true);
}

void GameWindow::rulesEventHandler(wxCommandEvent &event)  // wxGlade: MyFrame.<event_handler>
{
    wxInitAllImageHandlers();
    RulesWindow *rulesWindow = new RulesWindow(nullptr, wxID_ANY, wxEmptyString);
    rulesWindow->Show(true);
}

void GameWindow::quitEventHandler(wxCommandEvent &event)  // wxGlade: MyFrame.<event_handler>
{
    this->Close();
}

void GameWindow::showPanel(wxPanel *panel) {

    // if we are already showing the panel, we don't need to do anything
    if (this->_currentPanel == panel) {
        return;
    }

    // remove previous panel
    if (this->_currentPanel != nullptr) {
        this->_mainLayout->Detach(this->_currentPanel);
        this->_currentPanel->Show(false);
        this->_currentPanel = nullptr;
    }

    // add new panel
    this->_mainLayout->Add(panel, 0, wxALIGN_CENTER | wxALL, 20); // 20 pixel spacing
    panel->Show(true);
    this->_currentPanel = panel;

    // update layout
    this->_mainLayout->Layout();

    // update window size
    this->Fit();
}

void GameWindow::setStatus(const std::string &message) {
    this->_statusBar->SetStatusText(message, 0);
}

void GameWindow::updateBackground(wxColour color) {
    this->SetBackgroundColour(color);
    this->Refresh();
}

void GameWindow::OnClose(wxCloseEvent& event) {
    GameController::leaveGame();
    abort();
}

void GameWindow::updateScoreboard(std::vector<std::string> player_names, std::vector<std::string> player_ids, std::vector<int> scores) {
   
//  set up the scoreboard and its frame at the start
//  else the score gets added
    if (_player_ids.empty()){
        _table= new ScoreBoard(_scoreBoardWindow,player_names.size());
        wxBoxSizer *sizer = new wxBoxSizer(wxHORIZONTAL);
        sizer->Add(_table, 1, wxEXPAND | wxALL); 
        _scoreBoardWindow -> SetSizer(sizer);
        _scoreBoardWindow->SetClientSize(_table->GetBestSize());
        sizer->Fit(_scoreBoardWindow);
        _player_names = player_names;
        _player_ids = player_ids;
    }
    else{
        int i = 0;
        int j = 0;
        while (i < _player_ids.size()){
            if (_player_ids[i] != player_ids[j]){
                scores.insert(scores.begin() + j, 0);
                j--;
            }
            j++;
            i++;
        }
        _scores.push_back(scores);
    }
    _table->fill(player_ids,player_names,scores);
}

void GameWindow::showScoarboard(){
    _scoreBoardWindow->Show(true);
}
