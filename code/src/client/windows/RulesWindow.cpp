#include "RulesWindow.h"

RulesWindow::RulesWindow(wxWindow* parent, wxWindowID id, const wxString& title, const wxPoint& pos, const wxSize& size, long style):
        wxDialog(parent, id, title, pos, size, wxDEFAULT_DIALOG_STYLE) {
    // begin wxGlade: MyDialog::MyDialog
    SetSize(wxSize(900, 851));
    SetTitle(wxT("Rules"));
    panel_1 = new wxScrolledWindow(this, wxID_ANY, wxDefaultPosition, wxDefaultSize, wxTAB_TRAVERSAL);
    panel_1->SetScrollRate(10, 10);
    wxBoxSizer* sizer_2 = new wxBoxSizer(wxHORIZONTAL);
    wxStaticBitmap* bitmap_1 = new wxStaticBitmap(panel_1, wxID_ANY, wxBitmap(wxT("../assets/guide.jpg"), wxBITMAP_TYPE_ANY));
    sizer_2->Add(bitmap_1, 0, 0, 0);

    panel_1->SetSizer(sizer_2);
    Layout();
}
