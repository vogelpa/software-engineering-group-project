#ifndef SkullKingUI_SkullKing_H
#define SkullKingUI_SkullKing_H

#include <wx/wx.h>
#include "../windows/GameWindow.h"
#include "../GameController.h"


// Main app class
class SkullKing : public wxApp
{
public:
    virtual bool OnInit();
};


#endif //SkullKingUI_SkullKing_H
