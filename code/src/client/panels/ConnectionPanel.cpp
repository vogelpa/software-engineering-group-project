#include "ConnectionPanel.h"
#include "../uiElements/ImagePanel.h"
#include "../../common/network/default.conf"
#include "../GameController.h"


ConnectionPanel::ConnectionPanel(wxWindow* parent) : wxPanel(parent, wxID_ANY) {

    wxColor white = wxColor(255, 255, 255);
    this->SetBackgroundColour(white);

    wxBoxSizer* verticalLayout = new wxBoxSizer(wxVERTICAL);
    wxBoxSizer* buttonLayout = new wxBoxSizer(wxHORIZONTAL);

    ImagePanel* logo = new ImagePanel(this, "assets/Skull-King-logo.jpg", wxBITMAP_TYPE_ANY, wxDefaultPosition, wxSize(176*1.5, 250*1.5));
    verticalLayout->Add(logo, 0, wxALIGN_CENTER | wxTOP | wxLEFT | wxRIGHT, 10);

    this->_serverAddressField = new InputField(
        this, // parent element
        "Server address:", // label
        120, // width of label
        default_server_host, // default value (variable from "default.conf")
        240 // width of field
    );
    verticalLayout->Add(this->_serverAddressField, 0, wxTOP | wxLEFT | wxRIGHT, 10);

    this->_serverPortField = new InputField(
        this, // parent element
        "Server port:", // label
        120, // width of label
        wxString::Format("%i", default_port), // default value (variable from "default.conf")
        240 // width of field
    );
    verticalLayout->Add(this->_serverPortField, 0, wxTOP | wxLEFT | wxRIGHT, 10);

    this->_playerNameField = new InputField(
        this, // parent element
        "Player name:", // label
        120, // width of label
        "", // default value
        240 // width of field
    );
    verticalLayout->Add(this->_playerNameField, 0, wxTOP | wxLEFT | wxRIGHT, 10);

    wxButton* connectButton = new wxButton(this, wxID_ANY, "Connect", wxDefaultPosition, wxSize(100, 40));
    connectButton->Bind(wxEVT_BUTTON, [](wxCommandEvent& event) {
        GameController::connectToServer();
    });
    
    _server_started = false;
    wxButton* serverButton = new wxButton(this, wxID_ANY, "Create server", wxDefaultPosition, wxSize(120, 40));
    serverButton->Bind(wxEVT_BUTTON, [this](wxCommandEvent& event) {
        this->create_server();
    });
    
   
    buttonLayout->Add(serverButton, 0, wxALL, 10);
    buttonLayout->Add(connectButton, 0, wxALL, 10);
    verticalLayout->Add(buttonLayout, 0,wxALIGN_RIGHT);

    this->SetSizerAndFit(verticalLayout);
}

wxString ConnectionPanel::getServerAddress() {
    return this->_serverAddressField->getValue();
}

wxString ConnectionPanel::getServerPort() {
    return this->_serverPortField->getValue();
}

wxString ConnectionPanel::getPlayerName() {
    return this->_playerNameField->getValue();
}

void ConnectionPanel::create_server(){
    wxMessageDialog dialogBox = wxMessageDialog(nullptr, "Is gnome-terminal installed?", "", wxYES_NO|wxICON_QUESTION);
    if (!_server_started) {
        //if we press yes we start a server in a separate gnome-terminal
        if (dialogBox.ShowModal() == wxID_YES) {
            //system returns 0 if no errors occurred when execturing command
            if(!system("gnome-terminal -x ./Backend-Skull-King")){
                 _server_started = true;
             } else {
                 wxMessageBox("Could not start server. Please install gnome-terminal first\n"
                              "the command is: 'sudo apt-get install gnome-terminal'", "", wxICON_ERROR);
             }
        } else {
            wxMessageBox("Please install gnome-terminal first\n"
                         "the command is: 'sudo apt-get install gnome-terminal'", "", wxICON_INFORMATION);
        }
    }
    else {
        wxMessageBox("You have already started a server'", "", wxICON_ERROR);
    }
}