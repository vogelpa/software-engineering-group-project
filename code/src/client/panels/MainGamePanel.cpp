#include "MainGamePanel.h"
#include "../uiElements/ImagePanel.h"
#include "../GameController.h"


MainGamePanel::MainGamePanel(wxWindow* parent) : wxPanel(parent, wxID_ANY, wxDefaultPosition, wxSize(960, 680))
{ }

void MainGamePanel::buildGameState(GameState* gameState, Player* me) {

    // remove any existing UI
    this->DestroyChildren();
    this->SetForegroundColour(wxColour(0,0,0));

    std::vector<Player*> players = gameState->get_players();
    int numberOfPlayers;
    numberOfPlayers = players.size();

    // find our own Player object in the list of players
    int myPosition = -1;
    std::vector<Player*>::iterator it = std::find_if(players.begin(), players.end(), [me](const Player* x) {
        return x->get_id() == me->get_id();
    });
    if (it < players.end()) {
        me = *it;
        myPosition = it - players.begin();
    } else {
        GameController::showError("Game state error", "Could not find this Player among players of server game.");
        return;
    }

    double anglePerPlayer = MainGamePanel::twoPi / (double) numberOfPlayers;

    // show all other players
    for(int i = 1; i < numberOfPlayers; i++) {

        // get Player at i-th position after myself
        Player* otherPlayer = players.at((myPosition + i) % numberOfPlayers);

        double playerAngle = (double) i * anglePerPlayer;
        int side = (2 * i) - numberOfPlayers; // side < 0 => right, side == 0 => center, side > 0 => left

        this->buildOtherPlayerHand(gameState, otherPlayer, playerAngle, me, i);
        this->buildOtherPlayerLabels(gameState, otherPlayer, playerAngle, side);
    }

    // show both Card piles at the center
    this->buildCardPiles(gameState, me);

    // show turn indicator below Card piles
    this->buildTurnIndicator(gameState, me);

    // show our own Player
    this->buildThisPlayer(gameState, me);

    //adjust background to needed color
    this->updateBackground(gameState, me);

    // update layout
    this->Layout();
}

void MainGamePanel::buildOtherPlayerHand(GameState* gameState, Player* otherPlayer, double playerAngle, Player* me, int position) {

    // define the ellipse which represents the virtual Player circle
    double horizontalRadius = MainGamePanel::otherPlayerHandDistanceFromCenter * 1.4; // 1.4 to horizontally elongate players' circle
    double verticalRadius = MainGamePanel::otherPlayerHandDistanceFromCenter;

    // get this Player's position on that ellipse
    wxPoint handPosition = MainGamePanel::tableCenter;
    handPosition += this->getPointOnEllipse(horizontalRadius, verticalRadius, playerAngle);

    // calculate the number of card of each player based on the number of cards that you have
    int numberOfCards = me->get_nof_cards();
    
    int nof_Cards_player = gameState->get_played_pile()->get_cards().size();
    //if this is true, this means me has played->we need to add 1 card to the players who have not played yet
    if (nof_Cards_player>gameState->get_player_index(me) - gameState->get_starting_player_index()%gameState->get_players().size())
        numberOfCards++;
    //if this is true, this means otherPlayer has played->we need to remove 1 card of his hand
    if (nof_Cards_player>gameState->get_player_index(otherPlayer) - gameState->get_starting_player_index()%gameState->get_players().size())
        numberOfCards--;
    //to fix a problem where it would show the wrong number of cards at the end of a trick
    if (gameState->is_trick_finished())
        numberOfCards = me->get_nof_cards();

    if (numberOfCards > 0) {

        // get new bounds of image, as they increase when image is rotated
        wxSize boundsOfRotatedHand = this->getBoundsOfRotatedSquare(MainGamePanel::otherPlayerHandSize, playerAngle);
        handPosition -= boundsOfRotatedHand / 2;

        std::string handImage = "assets/hand_cards/hand_" + std::to_string(numberOfCards) + ".png";
        if(numberOfCards > 10) {
            handImage = "assets/hand_cards/hand_10.png";
        }
        new ImagePanel(this, handImage, wxBITMAP_TYPE_ANY, handPosition, boundsOfRotatedHand, playerAngle);

    } else if(numberOfCards == 0) {

        wxSize nonRotatedSize = wxSize((int) MainGamePanel::otherPlayerHandSize, (int) MainGamePanel::otherPlayerHandSize);
        handPosition -= nonRotatedSize / 2;

        auto icon_file = "assets/icon_" + std::to_string(((position + 1) % 4) + 1) + ".png";

        new ImagePanel(this, icon_file, wxBITMAP_TYPE_ANY, handPosition, nonRotatedSize);
    }
}

void MainGamePanel::buildOtherPlayerLabels(GameState* gameState, Player* otherPlayer, double playerAngle, int side) {

    long textAlignment = wxALIGN_CENTER;
    int labelOffsetX = 0;

    if(side < 0) { // right side
        textAlignment = wxALIGN_LEFT;
        labelOffsetX = 85;

    } else if(side > 0) { // left side
        textAlignment = wxALIGN_RIGHT;
        labelOffsetX = -85;
    }

    // define the ellipse which represents the virtual Player circle
    double horizontalRadius = MainGamePanel::otherPlayerLabelDistanceFromCenter * 1.25; // 1.25 to horizontally elongate players' circle (but less than the hands' circle)
    double verticalRadius = MainGamePanel::otherPlayerLabelDistanceFromCenter;

    // get this Player's position on that ellipse
    wxPoint labelPosition = MainGamePanel::tableCenter;
    labelPosition += this->getPointOnEllipse(horizontalRadius, verticalRadius, playerAngle);
    labelPosition += wxSize(labelOffsetX, 0);

    // if game has not yet started, we only have two lines
    if(!gameState->is_started()) {
        this->buildStaticText(otherPlayer->get_player_name(),labelPosition + wxSize(-100, -18),
                wxSize(200, 18),textAlignment,true);
        this->buildStaticText("waiting...",labelPosition + wxSize(-100, 0),
                wxSize(200, 18),textAlignment );
    } else {
        // Show other Player's name and score
        auto nameAndScoreText = otherPlayer->get_player_name() + " (Score: " + std::to_string(otherPlayer->get_score()) + ")";
        this->buildStaticText(nameAndScoreText,labelPosition + wxSize(-100, -27),
                wxSize(200, 18),textAlignment,true);

        // Show other Player's status label
        std::string statusText = "waiting...";
        bool bold = false;
        if(otherPlayer == gameState->get_current_player()) {
            statusText = "their turn";
            bold = true;
        }
        this->buildStaticText(statusText,labelPosition + wxSize(-100, -9),
                wxSize(200, 18),textAlignment,bold);

        std::string bid = std::to_string(otherPlayer->get_bid());
        //display "not placed" as bid if a players hasn't placed their bid yet
        if (bid == "-1"){
            bid = "not placed";
        }
        // Show other Player's bid and tricks
        std::string bidAndTricksText = "Bid: " + bid + "  Tricks: " + std::to_string(otherPlayer->get_tricks());

        this->buildStaticText(bidAndTricksText, labelPosition + wxSize(-100, 9), wxSize(200, 18), textAlignment,  false);
    }
}

void MainGamePanel::buildCardPiles(GameState* gameState, Player *me) {

    if(gameState->is_started()) {
        // see old cards:
        const double num_of_players = (gameState->get_players()).size();
        const std::vector<Card*> topCards = gameState->get_played_pile()->get_cards();

        if (!topCards.empty()) {
            for(;MainGamePanel::cards_on_field<num_of_players && MainGamePanel::cards_on_field<topCards.size();){
                // display card
                std::string fileName = getCardFileName(topCards[MainGamePanel::cards_on_field]);

                wxPoint discardPilePosition = MainGamePanel::tableCenter - ((num_of_players-1)/2)*MainGamePanel::discardPileOffsetForNextCard + MainGamePanel::discardPileOffset+
                                            MainGamePanel::discardPileOffsetForNextCard * MainGamePanel::cards_on_field;
                MainGamePanel::cards_on_field++;

                ImagePanel* discardPile = new ImagePanel(this, fileName, wxBITMAP_TYPE_ANY, discardPilePosition, MainGamePanel::cardSize,0,true);
                discardPile->SetToolTip("Discard pile");
            }
            MainGamePanel::cards_on_field=0;
        }
    } else {
        // if the game did not start yet, show a back side of a Card in the center (only for the mood)
        wxPoint cardPosition = MainGamePanel::tableCenter - (MainGamePanel::cardSize / 2);
        new ImagePanel(this, "assets/back.png", wxBITMAP_TYPE_ANY, cardPosition, MainGamePanel::cardSize,0 , true);
    }
}

void MainGamePanel::buildTurnIndicator(GameState *gameState, Player *me) {
    if(gameState->is_started() && gameState->get_current_player() != nullptr) {
        std::string turnIndicatorText = "It's " + gameState->get_current_player()->get_player_name() + "'s turn!";
        if(gameState->get_current_player() == me) {
            turnIndicatorText = "It's your turn!";
        }
        if(!gameState->are_bids_placed()){
            turnIndicatorText = "Waiting for all bids to be placed";
        }
        if (gameState->is_trick_finished()){
            turnIndicatorText = "Evaluating winner...";
        }
        wxPoint turnIndicatorPosition = MainGamePanel::tableCenter + MainGamePanel::turnIndicatorOffset;
        this->buildStaticText(turnIndicatorText, turnIndicatorPosition,
                              wxSize(200, 18), wxALIGN_CENTER, true);
    }
}

void MainGamePanel::buildThisPlayer(GameState* gameState, Player* me) {

    // Setup two nested box sizers, in order to align our Player's UI to the bottom center
    wxBoxSizer* outerLayout = new wxBoxSizer(wxHORIZONTAL);
    this->SetSizer(outerLayout);
    wxBoxSizer* innerLayout = new wxBoxSizer(wxVERTICAL);
    outerLayout->Add(innerLayout, 1, wxALIGN_BOTTOM);

    if(!gameState->is_started()) {
        // Show the label with our Player name
        wxStaticText* playerName = buildStaticText(me->get_player_name(), wxDefaultPosition,
                                                   wxSize(200, 18), wxALIGN_CENTER, true);
        innerLayout->Add(playerName, 0, wxALIGN_CENTER);

        // if the game has not yet started we say so
        wxStaticText* playerPoints = buildStaticText("Waiting for the game to start...",
                wxDefaultPosition,wxSize(200, 18),wxALIGN_CENTER);
        innerLayout->Add(playerPoints, 0, wxALIGN_CENTER | wxBOTTOM, 8);

        // show button that allows our Player to start the game
        wxButton* startGameButton = new wxButton(this, wxID_ANY, "Start Game!", wxDefaultPosition, wxSize(160, 64));
        startGameButton->Bind(wxEVT_BUTTON, [](wxCommandEvent& event) {
            GameController::startGame();
        });
        innerLayout->Add(startGameButton, 0, wxALIGN_CENTER | wxBOTTOM, 8);

    } else {
        // show our Player's name and score
        auto nameAndScoreText = me->get_player_name() + " (Score: " + std::to_string(me->get_score()) + ")";
        wxStaticText *playerPoints = buildStaticText(nameAndScoreText, wxDefaultPosition,wxSize(200, 18),wxALIGN_CENTER, true);
        innerLayout->Add(playerPoints, 0, wxALIGN_CENTER | wxBOTTOM, 8);

        std::string bidAndTricksText = "Bid: " + std::to_string(me->get_bid()) + " Tricks: " + std::to_string(me->get_tricks());
        wxStaticText *playerStats = buildStaticText(bidAndTricksText, wxDefaultPosition,wxSize(200, 18),wxALIGN_CENTER);
        innerLayout->Add(playerStats, 0, wxALIGN_CENTER | wxBOTTOM, 8);

        // display our Player's hand, if we have cards
        int numberOfCards = me->get_nof_cards();
        if (numberOfCards > 0) {

            // create horizontal layout for the individual hand cards of our Player
            wxBoxSizer *handLayout = new wxBoxSizer(wxHORIZONTAL);
            innerLayout->Add(handLayout, 0, wxALIGN_CENTER);

            wxSize scaledCardSize = MainGamePanel::cardSize;

            // Adjust Card size (if the number of cards does not fit on the screen)
            if (numberOfCards * (MainGamePanel::cardSize.GetWidth() + 8) >
                MainGamePanel::panelSize.GetWidth()) { // 8 -> 4 pixel padding on both sides
                int scaledCardWidth = (MainGamePanel::panelSize.GetWidth() / numberOfCards) - 8;
                double cardAspectRatio =
                        (double) MainGamePanel::cardSize.GetHeight() / (double) MainGamePanel::cardSize.GetWidth();
                int scaledCardHeight = (int) ((double) scaledCardWidth * cardAspectRatio);
                scaledCardSize = wxSize(scaledCardWidth, scaledCardHeight);
            }

            // Show all cards
            for (int i = 0; i < me->get_hand()->get_cards().size(); i++) {

                // display card
                Card *handCard = me->get_hand()->get_cards().at(i);
                std::string fileName = getCardFileName(handCard);
                ImagePanel *cardButton = new ImagePanel(this, fileName, wxBITMAP_TYPE_ANY, wxDefaultPosition, scaledCardSize, 0,true);

                if (gameState->get_current_player() == me) {
                    cardButton->SetToolTip("Play Card");
                    cardButton->SetCursor(wxCursor(wxCURSOR_HAND));
                    cardButton->Bind(wxEVT_LEFT_UP, [handCard](wxMouseEvent& event) {
                        GameController::playCard(handCard);
                    });
                }
                handLayout->Add(cardButton, 0, wxLEFT | wxRIGHT, 4);
            }
        }
    }
}

wxStaticText* MainGamePanel::buildStaticText(std::string content, wxPoint position, wxSize size, long textAlignment, bool bold) {
    wxStaticText* staticText = new wxStaticText(this, wxID_ANY, content, position, size, textAlignment);
    if(bold) {
        wxFont font = staticText->GetFont();
        font.SetWeight(wxFONTWEIGHT_BOLD);
        staticText->SetFont(font);
    }
    return staticText;
}

wxSize MainGamePanel::getBoundsOfRotatedSquare(double edgeLength, double rotationAngle) {
    double newEdgeLength = this->getEdgeLengthOfRotatedSquare(edgeLength, rotationAngle);
    //  wxSize(newEdgeLength*1.25, newEdgeLength);
    return wxSize(newEdgeLength, newEdgeLength);
}

double MainGamePanel::getEdgeLengthOfRotatedSquare(double originalEdgeLength, double rotationAngle) {
    return originalEdgeLength * (abs(sin(rotationAngle)) + abs(cos(rotationAngle)));
}

wxPoint MainGamePanel::getPointOnEllipse(double horizontalRadius, double verticalRadius, double angle) {
    return wxPoint((int) (sin(angle) * horizontalRadius), (int) (cos(angle) * verticalRadius));
}

std::string MainGamePanel::getCardFileName(Card *card) {
    //colors: 0=none, 1=blue, 2=red, 3=yellow, 4=black
    //values go from 0 to 16, 0=flag, 1-13=cards, 14=mermaid, 15=pirate, 16=skullking, 17=scary_mary
    auto cardTypeNumberString = std::to_string(card->get_type());
    auto cardValueNumberString = std::to_string(card->get_value());

    return "assets/cards/" + cardTypeNumberString + "_" + cardValueNumberString + ".png";
}

void MainGamePanel::updateBackground(GameState* gameState, Player* me){
    int state = gameState->get_played_pile()->get_currentType();
    wxColor background = wxColor(200, 200, 200);

    switch (state) {
        case 1: //blue
            background = wxColor(199, 199, 212);
            break;
        case 2: //red
            background = wxColor(212, 199, 199);
            break;
        case 3: //yellow
            background = wxColor(212, 210, 199);
            break;
        case 4: //black
            background = wxColor(189, 189, 189);
            break;
        default:
            background = wxColor(220, 220, 220);
    }
    this->SetBackgroundColour(background);
    this->Refresh();
}