#ifndef BIDPANEL_H
#define BIDPANEL_H

#include <wx/wxprec.h>
#include <wx/listctrl.h>
#include <wx/sizer.h>
#include <iostream>
#include <string>
#ifndef WX_PRECOMP
#include <wx/wx.h>
#endif

#include "../../common/game_state/GameState.h"
#include "MainGamePanel.h"

class BidPanel : public wxPanel{
    public:
        explicit BidPanel(wxWindow* parent);
        void makeBid(GameState* gameState, Player* me, int round=1);
        
    private:
        std::string getCardFileName(Card *pCard);
        wxSize const cardSize = wxSize(80*1.4, 124*1.4);
        wxSize const panelSize = wxSize(960, 680);
        int _round;
        wxBoxSizer * _sizer;
};


#endif // BIDPANEL_H