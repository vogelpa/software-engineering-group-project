#include "BidPanel.h"
#include "../uiElements/ImagePanel.h"
#include "../GameController.h"

//constructor
BidPanel::BidPanel(wxWindow* parent) : wxPanel(parent, wxID_ANY, wxDefaultPosition, wxSize(960, 340)){
}


//function to create the bid panel with all the buttons. 
void BidPanel::makeBid(GameState* gameState, Player* me, int round){
    
    //resets the panel
    this->DestroyChildren();

    std::vector<Player*> players = gameState->get_players();
    int numberOfPlayers = players.size();
    int startingPlayer= gameState->get_player_index(gameState->get_current_player());
    
    // find our own Player object in the list of players
    int myPosition = -1;
    std::vector<Player*>::iterator it = std::find_if(players.begin(), players.end(), [me](const Player* x) {
        return x->get_id() == me->get_id();
    });
    if (it < players.end()) {
        me = *it;
        myPosition = it - players.begin();
    } else {
        GameController::showError("Game state error", "Could not find this Player among players of server game.");
        return;
    }

    wxBoxSizer *layout=new wxBoxSizer(wxVERTICAL);
    _round = round;
    int numberOfCards =me->get_nof_cards();

    //display bid buttons
    _sizer = new wxBoxSizer(wxHORIZONTAL);
    for(int i=0;i<=_round;i++){
        wxButton *bid_field = new wxButton(this,i,""+std::to_string(i)+"",wxDefaultPosition, BidPanel::cardSize);
        _sizer-> Add(bid_field,1, wxLEFT| wxTOP | wxRIGHT, 2);
        bid_field->SetToolTip("Place Bid");
        bid_field->SetCursor(wxCursor(wxCURSOR_HAND));
        bid_field->Bind(wxEVT_LEFT_UP, [i](wxMouseEvent& event) {
            GameController::placeBid(i);}
        );
    }

    //show hand cards
    if (numberOfCards > 0) {

        // create horizontal layout for the individual hand cards of our Player
        wxBoxSizer *handLayout = new wxBoxSizer(wxHORIZONTAL);
        layout->Add(handLayout, 0, wxALIGN_CENTER | wxBOTTOM| wxTOP, 100);

        wxSize scaledCardSize = BidPanel::cardSize;

        // Adjust Card size (if the number of cards does not fit on the screen)
        if (numberOfCards * (BidPanel::cardSize.GetWidth() + 8) >
            BidPanel::panelSize.GetWidth()) { // 8 -> 4 pixel padding on both sides
            int scaledCardWidth = (BidPanel::panelSize.GetWidth() / numberOfCards) - 8;
            double cardAspectRatio =
                    (double) BidPanel::cardSize.GetHeight() / (double) BidPanel::cardSize.GetWidth();
            int scaledCardHeight = (int) ((double) scaledCardWidth * cardAspectRatio);
            scaledCardSize = wxSize(scaledCardWidth, scaledCardHeight);
        }

        // Show all cards
        for (int i = 0; i <me->get_hand()->get_cards().size(); i++) {

            Card *handCard = me->get_hand()->get_cards().at(i);
            std::string fileName = getCardFileName(handCard);

            ImagePanel *cardButton = new ImagePanel(this, fileName, wxBITMAP_TYPE_ANY, wxDefaultPosition, scaledCardSize, 0, true);
            handLayout->Add(cardButton, 0, wxLEFT | wxRIGHT, 4);
        }
    }
    
    //indicate your turn to help make tactical decision
    wxStaticText* staticText;
    int turnNumber =((myPosition-startingPlayer+numberOfPlayers)%numberOfPlayers) + 1;
    switch(turnNumber){
        case 1: staticText = new wxStaticText(this, wxID_ANY, "you are the " + std::to_string(turnNumber)+ "st player to play");
            break;
        case 2: staticText = new wxStaticText(this, wxID_ANY, "you are the " + std::to_string(turnNumber)+ "nd player to play");
            break;
        case 3: staticText = new wxStaticText(this, wxID_ANY, "you are the " + std::to_string(turnNumber)+ "rd player to play");
            break;
        default: staticText = new wxStaticText(this, wxID_ANY, "you are the " + std::to_string(turnNumber)+ "th player to play");
            break;
    }

    layout->Add(staticText,0,wxALIGN_CENTER);
    layout->Add(BidPanel::_sizer, 0,wxALIGN_CENTER | wxTOP, 100);
    this->SetSizerAndFit(layout);
    this->Layout();
}

//get the card image
std::string BidPanel::getCardFileName(Card *card) {
    auto cardTypeNumberString = std::to_string(card->get_type());
    auto cardValueNumberString = std::to_string(card->get_value());

    return "assets/cards/" + cardTypeNumberString + "_" + cardValueNumberString + ".png";
}



