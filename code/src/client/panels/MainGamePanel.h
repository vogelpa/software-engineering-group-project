#ifndef SKULLKING_CLIENT_MAINGAMEPANEL_H
#define SKULLKING_CLIENT_MAINGAMEPANEL_H

#include <wx/wx.h>
#include "../../common/game_state/GameState.h"


class MainGamePanel : public wxPanel {

public:
    explicit MainGamePanel(wxWindow* parent);

    void buildGameState(GameState* gameState, Player* me);


private:
    void buildOtherPlayerHand(GameState* gameState, Player* otherPlayer, double playerAngle, Player* me, int position);
    void buildOtherPlayerLabels(GameState* gameState, Player* otherPlayer, double playerAngle, int side);
    void buildCardPiles(GameState* gameState, Player *me);
    void buildTurnIndicator(GameState* gameState, Player* me);
    void buildThisPlayer(GameState* gameState, Player* me);
    void updateBackground(GameState* gameState, Player* me);

    wxStaticText* buildStaticText(std::string content, wxPoint position, wxSize size, long textAlignment, bool bold = false);

    wxSize getBoundsOfRotatedSquare(double edgeLength, double rotationAngle);
    double getEdgeLengthOfRotatedSquare(double originalEdgeLength, double rotationAngle);

    wxPoint getPointOnEllipse(double horizontalRadius, double verticalRadius, double angle);

    // define key constant layout values
    wxSize const panelSize = wxSize(960, 680); // also set in the constructor implementation
    wxPoint const tableCenter = wxPoint(480, 300);
    wxSize const cardSize = wxSize(80*1.4, 124*1.4);

    double const otherPlayerHandSize = 160.0;
    double const otherPlayerHandDistanceFromCenter = 180.0;
    double const otherPlayerLabelDistanceFromCenter = 275.0;

    wxPoint const discardPileOffset = wxPoint(-40*1.4, -42*1.4);
    // dummy integer, to be removed later;
    int cards_on_field =0;
    wxPoint const discardPileOffsetForNextCard = wxPoint(-20*2,0);
    wxPoint const turnIndicatorOffset = wxPoint(-100, 98*1.4);

    double const twoPi = 6.28318530718;

    std::string getCardFileName(Card *pCard);
};


#endif //SKULLKING_CLIENT_MAINGAMEPANEL_H
