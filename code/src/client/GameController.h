#ifndef SkullKingUI_GAMECONTROLLER_H
#define SkullKingUI_GAMECONTROLLER_H

#include "windows/GameWindow.h"
#include "panels/ConnectionPanel.h"
#include "panels/MainGamePanel.h"
#include "panels/BidPanel.h"
#include "network/ResponseListenerThread.h"
#include "../common/game_state/GameState.h"


class GameController {

public:
    static void init(GameWindow* gameWindow);

    static void connectToServer();
    static void updateGameState(GameState* newGameState);
    static void startGame();
    static void playCard(Card* cardToPlay);
    static void placeBid(int bid_amount);
    static void leaveGame();


    static wxEvtHandler* getMainThreadEventHandler();
    static void showError(const std::string& title, const std::string& message);
    static void showStatus(const std::string& message);
    static void showNewRoundMessage(GameState* oldGameState, GameState* newGameState);
    static void showGameOverMessage();
    static Card* showScaryMaryMessage();
    static void atStartOfRound();
    
private:
    static GameWindow* _gameWindow;
    static ConnectionPanel* _connectionPanel;
    static MainGamePanel* _mainGamePanel;
    static BidPanel* _bidPanel;

    static Player* _me;
    static GameState* _currentGameState;
    static std::tuple<std::string, std::string, std::vector<int>>* _scoreboard;

};


#endif //SkullKingUI_GAMECONTROLLER_H
