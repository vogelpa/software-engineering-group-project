#include "GameController.h"
#include "../common/network/requests/join_game_request.h"
#include "../common/network/requests/start_game_request.h"
#include "../common/network/requests/place_bid_request.h"
#include "../common/network/requests/play_card_request.h"
#include "../common/network/requests/leave_game_request.h"
#include "network/ClientNetworkManager.h"

// initialize static members
GameWindow* GameController::_gameWindow = nullptr;
ConnectionPanel* GameController::_connectionPanel = nullptr;
MainGamePanel* GameController::_mainGamePanel = nullptr;
BidPanel* GameController::_bidPanel = nullptr;
Player* GameController::_me = nullptr;
GameState* GameController::_currentGameState = nullptr;

void GameController::init(GameWindow* gameWindow) {

    GameController::_gameWindow = gameWindow;
    // try to debug white screen, if still present remove
    _gameWindow->SetSize(wxSize(1082, 872));

    // Set up main panels
    GameController::_connectionPanel = new ConnectionPanel(gameWindow);
    GameController::_mainGamePanel = new MainGamePanel(gameWindow);
    GameController::_bidPanel= new BidPanel(gameWindow);

    // Hide all panels
    GameController::_connectionPanel->Show(false);
    GameController::_mainGamePanel->Show(false);
    GameController::_bidPanel->Show(false);

    // Only show connection panel at the start of the game
    GameController::_gameWindow->showPanel(GameController::_connectionPanel);

    // Set status bar
    GameController::showStatus("Not connected");
}

void GameController::connectToServer() {

    // get values form UI input fields
    wxString inputServerAddress = GameController::_connectionPanel->getServerAddress().Trim();
    wxString inputServerPort = GameController::_connectionPanel->getServerPort().Trim();
    wxString inputPlayerName = GameController::_connectionPanel->getPlayerName().Trim();

    // check that all values were provided
    if(inputServerAddress.IsEmpty()) {
        GameController::showError("Input error", "Please provide the server's address");
        return;
    }
    if(inputServerPort.IsEmpty()) {
        GameController::showError("Input error", "Please provide the server's port number");
        return;
    }
    if(inputPlayerName.IsEmpty()) {
        GameController::showError("Input error", "Please enter your desired Player name");
        return;
    }

    // convert host from wxString to std::string
    std::string host = inputServerAddress.ToStdString();

    // convert port from wxString to uint16_t
    unsigned long portAsLong;
    if(!inputServerPort.ToULong(&portAsLong) || portAsLong > 65535) {
        GameController::showError("Connection error", "Invalid port");
        return;
    }
    uint16_t port = (uint16_t) portAsLong;

    // convert Player name from wxString to std::string
    std::string playerName = inputPlayerName.ToStdString();

    // connect to network
    ClientNetworkManager::init(host, port);

    // send request to join game
    GameController::_me = new Player(playerName);
    join_game_request request = join_game_request(GameController::_me->get_id(), GameController::_me->get_player_name());
    ClientNetworkManager::sendRequest(request);

}

void GameController::updateGameState(GameState* newGameState) {

    // the existing game state is now old
    GameState* oldGameState = GameController::_currentGameState;

    // save the new game state as our current game state
    GameController::_currentGameState = newGameState;
    bool new_round = false;
    if(oldGameState != nullptr) {

        // check if a new round started, and display message accordingly
        if(oldGameState->get_round_counter() >= 0 && oldGameState->get_round_counter() < newGameState->get_round_counter()) {
            new_round = true;
            if(oldGameState->get_round_counter() > 0)
                GameController::showNewRoundMessage(oldGameState, newGameState);
        }
    }

    if(GameController::_currentGameState->is_finished()) {
        //we don't want to show the round over message when the game ends due to too many players leaving
        if(newGameState->get_players().size() > 1) {
            GameController::showNewRoundMessage(oldGameState, newGameState);
        }
        GameController::showGameOverMessage();
    }
    // delete the old game state, we don't need it anymore
    delete oldGameState;

    // make sure we are showing the main game panel in the window (if we are already showing it, nothing will happen)
    GameController::_gameWindow->showPanel(GameController::_mainGamePanel);

    // show menubar at the top for scoreboard, rules, quit
    GameController::_gameWindow->showMenubar();

    int state = GameController::_currentGameState->get_played_pile()->get_currentType();
    wxColor background = wxColor(200, 200, 200);
   switch (state) {
        case 1: //blue
            background = wxColor(199, 199, 212);
            break;
        case 2: //red
            background = wxColor(212, 199, 199);
            break;
        case 3: //yellow
            background = wxColor(212, 210, 199);
            break;
        case 4: //black
            background = wxColor(189, 189, 189);
            break;
        default:
            background = wxColor(220, 220, 220);
    }
    GameController::_gameWindow->updateBackground(background);

    // command the main game panel to rebuild itself, based on the new game state
    GameController::_mainGamePanel->buildGameState(GameController::_currentGameState, GameController::_me);

    // find our own Player object in the list of players
    int myPosition = -1;
    std::vector<Player*>::iterator it = std::find_if(_currentGameState->get_players().begin(), _currentGameState->get_players().end(), [](const Player* x) {
        return x->get_id() == _me->get_id();
    });
    if (it < _currentGameState->get_players().end()) {
        _me = *it;
        myPosition = it - _currentGameState->get_players().begin();
    }

    //if we haven't placed our bid yet, show bid panel
    if(new_round || _me->get_bid() == -1){
        GameController::_bidPanel->makeBid(GameController::_currentGameState, GameController::_me, GameController::_currentGameState->get_round_counter());
        GameController::_gameWindow->showPanel(GameController::_bidPanel);
    }
    else
        GameController::_gameWindow->showPanel(GameController::_mainGamePanel);
}

void GameController::atStartOfRound(){
    GameController::_gameWindow->showPanel(GameController::_mainGamePanel);
}

void GameController::startGame() {
    start_game_request request = start_game_request(GameController::_currentGameState->get_id(), GameController::_me->get_id());
    ClientNetworkManager::sendRequest(request);
}

void GameController::playCard(Card* cardToPlay) {
    if(cardToPlay->get_value() == 17){
        cardToPlay = showScaryMaryMessage();
    }
    play_card_request request = play_card_request(GameController::_currentGameState->get_id(), GameController::_me->get_id(), cardToPlay->get_id());
    ClientNetworkManager::sendRequest(request);
}
void GameController::placeBid(int bid_amount) {
    place_bid_request request = place_bid_request(GameController::_currentGameState->get_id(), GameController::_me->get_id(), bid_amount);
    ClientNetworkManager::sendRequest(request);
}

void GameController::leaveGame() {
    leave_game_request request = leave_game_request(GameController::_currentGameState->get_id(), GameController::_me->get_id());
    ClientNetworkManager::sendRequest(request);
}

wxEvtHandler* GameController::getMainThreadEventHandler() {
    return GameController::_gameWindow->GetEventHandler();
}

void GameController::showError(const std::string& title, const std::string& message) {
    wxMessageBox(message, title, wxICON_ERROR);
}

void GameController::showStatus(const std::string& message) {
    GameController::_gameWindow->setStatus(message);
}

void GameController::showNewRoundMessage(GameState* oldGameState, GameState* newGameState) {
    std::string title = "Round Completed";
    std::string message = "The players gained the following points:\n";
    std::string buttonLabel = "Start next round";
    //show a different button text if game is over
    if (newGameState->is_finished()){
        buttonLabel = "Show final results";
    }
    //needed for update scoreboard
    std::vector<int> scores;
    std::vector<std::string> player_names;
    std::vector<std::string> player_ids;
    // add the point differences of all players to the messages
    for(int i = 0; i < newGameState->get_players().size(); i++) {
        int j = i;
        Player* newPlayerState = newGameState->get_players().at(i);

        //if players have left we need to adjust the indexing
        for(auto& k: oldGameState->get_players_to_remove()){
            if (oldGameState->get_player_index(newPlayerState) >= oldGameState->get_player_index(k)) {
                ++j;
            }
        }
        Player* oldPlayerState = oldGameState->get_players().at(j);

        int scoreDelta = newPlayerState->get_score() - oldPlayerState->get_score();
        //save score difference for each player (with respective player id)
        scores.push_back(scoreDelta);
        player_names.push_back(newPlayerState->get_player_name());
        player_ids.push_back(newPlayerState->get_id());

        std::string scoreText = std::to_string(scoreDelta);
        if(scoreDelta > 0) {
            scoreText = "+" + scoreText;
        }

        std::string playerName = newPlayerState->get_player_name();
        if(newPlayerState->get_id() == GameController::_me->get_id()) {
            playerName = "You";
        }
        message += "\n" + playerName + ":     " + scoreText;
    }

    //send update to scoreboard
    _gameWindow->updateScoreboard(player_names, player_ids, scores);
    
    wxMessageDialog dialogBox = wxMessageDialog(nullptr, message, title, wxICON_NONE);
    dialogBox.SetOKLabel(wxMessageDialog::ButtonLabel(buttonLabel));
    dialogBox.ShowModal();
}

void GameController::showGameOverMessage() {
    std::string title = "Game Over!";
    std::string message = "Final score:\n";
    std::string buttonLabel = "Close Game";

    // sort players by score
    std::vector<Player*> players = GameController::_currentGameState->get_players();
    std::sort(players.begin(), players.end(), [](const Player* a, const Player* b) -> bool {
        return a->get_score() > b->get_score();
    });

    // list all players
    for(int i = 0; i < players.size(); i++) {
        Player* playerState = players.at(i);
        std::string scoreText = std::to_string(playerState->get_score());

        // first entry is the winner
        std::string winnerText = "";
        if(i == 0) {
            winnerText = "     Winner!";
        }

        std::string playerName = playerState->get_player_name();
        if(playerState->get_id() == GameController::_me->get_id()) {
            playerName = "You";
            if(i == 0) {
                winnerText = "     You won!!!";
            }
        }
        message += "\n" + playerName + ":     " + scoreText + winnerText;
    }
    _gameWindow->showScoarboard();
    wxMessageDialog dialogBox = wxMessageDialog(nullptr, message, title, wxICON_NONE);
    dialogBox.SetOKLabel(wxMessageDialog::ButtonLabel(buttonLabel));
    int buttonClicked = dialogBox.ShowModal();
    if(buttonClicked == wxID_OK) {
        GameController::_gameWindow->Close();
    }

}

Card* GameController::showScaryMaryMessage() {
    Card* flag_pirate = _currentGameState->get_scary_mary()[1];;
    std::string title = "Scary Mary";
    std::string message = "Play Scary Mary as:\n";
    std::string flagLabel = "Flag";
    std::string pirateLabel = "Pirate";

    wxMessageDialog dialogBox = wxMessageDialog(nullptr, message, title, wxYES_NO|wxICON_NONE);
    dialogBox.SetYesNoLabels(wxMessageDialog::ButtonLabel(pirateLabel),wxMessageDialog::ButtonLabel(flagLabel));

    //if we press flag (no) we set the card to send to flag, else pirate
    if(dialogBox.ShowModal() == wxID_NO) {
        flag_pirate = _currentGameState->get_scary_mary()[0];
    }

    return flag_pirate;
}