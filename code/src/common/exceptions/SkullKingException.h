#ifndef SKULLKING_SKULLKINGEXCEPTION_H
#define SKULLKING_SKULLKINGEXCEPTION_H

#include <string>

class SkullkingException : public std::exception {
private:
    std::string _msg;
public:
    explicit SkullkingException(const std::string& message) : _msg(message) { };

    const char* what() const noexcept override {
        return _msg.c_str();
    }
};

#endif //SKULLKING_SKULLKINGEXCEPTION_H
