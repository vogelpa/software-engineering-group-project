#ifndef SkullKing_DRAWPILE_H
#define SkullKing_DRAWPILE_H

#include "Card.h"
#include <vector>
#include <string>
#include <algorithm>
#include "../../serialization/serializable.h"
#include "../../serialization/unique_serializable.h"
#include "../../serialization/serializable_value.h"
#include "../../game_state/player/Player.h"
#include "../../../../rapidjson/include/rapidjson/document.h"

class DrawPile : public unique_serializable {
private:
    std::vector<Card*> _cards;

    //Deserialization constructor
    DrawPile(std::string id, std::vector<Card*>& cards);

    // from_diff constructor
    explicit DrawPile(std::string id);

    // pile functions
    void shuffleCards();
    void reset();

public:
    // constructors
    DrawPile();
    explicit DrawPile(std::vector<Card*>& cards);
    ~DrawPile();

// accessors
    bool is_empty() const noexcept;
    int get_nof_cards() const noexcept;

#ifdef SKULLKING_SERVER
    // state update functions
    void setup_game(std::string& err);  // Fills the stack with all cards of the game
    bool distributeCards(Player *player, int num_cards, std::string &err);
#endif

// serialization
    virtual void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const override;
    static DrawPile* from_json(const rapidjson::Value& json);
};

#endif //SkullKing_DRAWPILE_H