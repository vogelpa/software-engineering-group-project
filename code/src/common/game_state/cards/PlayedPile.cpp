#include "PlayedPile.h"
#include "../../serialization/vector_utils.h"
#include "../../exceptions/SkullKingException.h"

PlayedPile::PlayedPile(std::string id) : unique_serializable(id) {
    this->_currentType = new serializable_value<int>(0);
 }

PlayedPile::PlayedPile(std::string id, std::vector<Card*> &cards,
                           serializable_value<int>* currentType):
        unique_serializable(std::move(id)),
        _cards(cards),
        _currentType(currentType)
{ }

PlayedPile::PlayedPile() : unique_serializable() {
    this->_currentType = new serializable_value<int>(0);
}

PlayedPile::~PlayedPile() {
    for (auto & card : _cards) {
        delete card;
    }
    _cards.clear();
    delete _currentType;
    _currentType = nullptr;
}

std::vector<Card*> PlayedPile::get_cards() const {
    return _cards;
}

//checks whether the player is allowed to play the chosen card
bool PlayedPile::can_play(Player* player, const Card* card) {
    int color = card->get_type();
    int currentType = _currentType->get_value();
    //if no card of color has been played yet, any card is allowed
    if(_cards.empty() || currentType == 0){
        _currentType->set_value(color);
        return true;
    }
    //playing a card of type 0 (pirate, flag, mermaid) is always allowed
    else if (color == currentType || color == 0){
        return true;
    }
    else{
        const std::vector<Card*> cards = player->get_hand()->get_cards();
        for (auto card : cards){
            //if the player has a card of type asked color, he can't play a card of a different color
            if(card->get_type() == currentType){
                return false;
            }
        }
        return true;
    }
}

//returns winner of trick (relative to starting player) and bonus points for the trick
std::pair<int,int> PlayedPile::determine_winner_of_trick() {
  int current_winner = 0;
  for(int i = 1; i < _cards.size(); i++){
      //colors: 0=none, 1=blue, 2=red, 3=yellow, 4=black
      //values go from 0 to 16, 0=flag, 1-13=cards, 14=mermaid, 15=pirate, 16=skullking
      switch (_cards[i]->get_type()){
          case 0:
              //pirate, has strictly greater value than colors
              if (_cards[current_winner]->get_value() < _cards[i]->get_value()){
                current_winner = i;
              }
              break;
          case 1:
          case 2:
          case 3:
            //color (red,blue,yellow), only better when same color & higher value, or flag
            if ((_cards[current_winner]->get_type() == _cards[i]->get_type())
                && (_cards[current_winner]->get_value() < _cards[i]->get_value())){
               current_winner = i;
            }
            //flag
            else if (_cards[current_winner]->get_value() == 0){
                current_winner = i;
            }
            break;
          case 4:
              //black, better than any color, with same color only if higher value
              if ((_cards[current_winner]->get_type() == _cards[i]->get_type())
                  && (_cards[current_winner]->get_value() < _cards[i]->get_value())){
                current_winner = i;
              }
              //black better than any color
              else if (_cards[current_winner]->get_type() == 1 ||
                  _cards[current_winner]->get_type() == 2 ||
                  _cards[current_winner]->get_type() == 3){
                  current_winner = i;
              }
              //flag
              else if (_cards[current_winner]->get_value() == 0){
                  current_winner = i;
              }
              break;
      }
  }
    //check for bonus points
    int bonus_points = 0;
    if(_cards[current_winner]->get_value() == 16){
        for(int i = 0; i < _cards.size(); i++){
            //for each pirate get 30 potential bonus points
            if(_cards[i]->get_value() == 15){
                bonus_points += 30;
            }
            //mermaid beats skullking, 50 potential bonus points
            if(_cards[i]->get_value() == 14){
                current_winner = i;
                bonus_points += 50;
                break;
            }
        }
    }
    return std::pair<int,int>(current_winner, bonus_points);
}

int PlayedPile::get_currentType() const noexcept {
    return _currentType->get_value();
}

#ifdef SKULLKING_SERVER
//removes all cards from the played pile
void PlayedPile::reset(std::string &err) {
    for (int i = 0; i < _cards.size(); i++) {
        delete _cards[i];
    }
    _cards.clear();
    _currentType->set_value(0);
}

bool PlayedPile::try_play(const std::string& card_id, Card* played_card, Player* Player, std::string& err) {
    if (can_play(Player, played_card)) {
        _cards.push_back(played_card);
        return true;
    }
    //error message telling player why he is not allowed to play card of color xy
    else {
        std::string color;
        std::string color2;
        int typenr = played_card->get_type();
        int typenr2 = get_currentType();
        switch (typenr){
            case 1:
                color = "blue";
                break;
            case 2:
                color = "red";
                break;
            case 3:
                color = "yellow";
                break;
            case 4:
                color = "black";
        }
        switch (typenr2){
            case 1:
                color2 = "blue";
                break;
            case 2:
                color2 = "red";
                break;
            case 3:
                color2 = "yellow";
                break;
            case 4:
                color2 = "black";
        }

        err = "You tried to play a " + color + " card but you have a " + color2 +
              " card in hand. You must play a " + color2 + " card or one without a color.";
    }
    return false;
}

#endif

PlayedPile *PlayedPile::from_json(const rapidjson::Value &json) {
    if (json.HasMember("id") 
        && json.HasMember("cards")
        && json.HasMember("currentType"))
    {
        std::vector<Card*> deserialized_cards = std::vector<Card*>();
        for (auto &serialized_card : json["cards"].GetArray()) {
            deserialized_cards.push_back(Card::from_json(serialized_card.GetObject()));
        }

        return new PlayedPile(json["id"].GetString(),
                                deserialized_cards,
                                serializable_value<int>::from_json(json["currentType"].GetObject()));
    } else {
       throw SkullkingException("Could not parse DrawPile from json. 'id' or 'cards' or 'currentType' were missing.");
    }
}

void PlayedPile::write_into_json(rapidjson::Value &json,
                                  rapidjson::MemoryPoolAllocator<rapidjson::CrtAllocator> &allocator) const {
    unique_serializable::write_into_json(json, allocator);
    
    json.AddMember("cards", vector_utils::serialize_vector(_cards, allocator), allocator);

    rapidjson::Value currentType_val(rapidjson::kObjectType);
    _currentType->write_into_json(currentType_val, allocator);
    json.AddMember("currentType", currentType_val, allocator);
}