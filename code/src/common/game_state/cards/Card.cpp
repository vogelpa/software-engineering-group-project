#include "Card.h"

#include <utility>

#include "../../exceptions/SkullKingException.h"

Card::Card(std::string id) : unique_serializable(std::move(id)) { }

Card::Card(std::string id, serializable_value<int> *val, serializable_value<int> *type)
        : unique_serializable(id), _value(val), _type(type)
{ }

Card::Card(int val, int type) :
        unique_serializable(),
        _value(new serializable_value<int>(val)),
        _type(new serializable_value<int>(type))
{ }

Card::~Card() = default;

int Card::get_value() const noexcept {
    return _value->get_value();
}

int Card::get_type() const noexcept {
    return _type->get_value();
}

Card *Card::from_json(const rapidjson::Value &json) {
    if (json.HasMember("id") 
        && json.HasMember("value")
        && json.HasMember("type")) {
        return new Card(json["id"].GetString(),
                        serializable_value<int>::from_json(json["value"].GetObject()),
                        serializable_value<int>::from_json(json["type"].GetObject()));
    } else {
       throw SkullkingException("Could not parse json of Card. Was missing 'id', 'val' or 'type'.");
    }
}

void Card::write_into_json(rapidjson::Value &json, rapidjson::Document::AllocatorType& allocator) const {
    unique_serializable::write_into_json(json, allocator);

    rapidjson::Value value_val(rapidjson::kObjectType);
    _value->write_into_json(value_val, allocator);
    json.AddMember("value", value_val, allocator);

    rapidjson::Value type_val(rapidjson::kObjectType);
    _type->write_into_json(type_val, allocator);
    json.AddMember("type", type_val, allocator);   
}