// Card values:
// 0: Flag, 1 - 13: Numbers,  14: Mermaid, 15: Pirate, 16: Skull King
//
// Card types:
// 0: None, 1: Blue, 2: Red, 3: Yellow, 4: Black

#ifndef SkullKing_CARD_H
#define SkullKing_CARD_H

#include <string>
#include "../../serialization/unique_serializable.h"
#include "../../serialization/serializable_value.h"
#include "../../../../rapidjson/include/rapidjson/document.h"

class Card : public unique_serializable {
private:
    serializable_value<int>* _value;
    serializable_value<int>* _type;

    // from_diff constructor
    explicit Card(std::string id);
    // deserialization constructor
    Card(std::string id, serializable_value<int>* val, serializable_value<int>* type);
public:
    Card(int val, int type);
    ~Card();

// accessors
    int get_value() const noexcept;
    int get_type() const noexcept;

// serializable interface
    void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const override;
    static Card* from_json(const rapidjson::Value& json);
};

#endif //SkullKing_CARD_H