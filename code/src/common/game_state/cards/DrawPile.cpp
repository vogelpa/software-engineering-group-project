#include "DrawPile.h"

#include <utility>


#include "../../serialization/vector_utils.h"
#include "../../exceptions/SkullKingException.h"

// deserialization constructor
DrawPile::DrawPile(std::string id, std::vector<Card*> &cards)
        : unique_serializable(std::move(id)),
          _cards(cards)
{ }

// from_diff constructor
DrawPile::DrawPile(std::string id) : unique_serializable(std::move(id)) { }

DrawPile::DrawPile(std::vector<Card*> &cards)
        : unique_serializable(), _cards(cards)
{ }

DrawPile::DrawPile() : unique_serializable() { }

DrawPile::~DrawPile() {
    for (Card* & _card : _cards) {
        delete _card;
    }
    _cards.clear();
}

void DrawPile::shuffleCards() {
    std::shuffle(_cards.begin(), _cards.end(), std::mt19937(std::random_device()()));
}

bool DrawPile::is_empty() const noexcept  {
    return _cards.empty();
}

int DrawPile::get_nof_cards() const noexcept  {
    return _cards.size();
}

void DrawPile::reset() {
    for (auto & _card : _cards) delete _card;
    _cards.clear();
}

#ifdef SKULLKING_SERVER
void DrawPile::setup_game(std::string &err) {
    // remove all cards
    reset();

    _cards.reserve(66);

    // add cards of different colors
    for (int t = 1; t <= 4; ++t) {
        for (int v = 1; v <= 13; ++v) {
            _cards.push_back(new Card(v, t));
        }
    }
    // 5 flags
    for (int i = 0; i < 5; ++i) {
        _cards.push_back(new Card(0, 0));
    }
    // 5 pirates
    for (int i = 0; i < 5; ++i) {
        _cards.push_back(new Card(15, 0));
    }
    //scary mary
    _cards.push_back(new Card(17,0));

    // 2 mermaids
    _cards.push_back(new Card(14, 0));
    _cards.push_back(new Card(14, 0));

    // 1 skull king
    _cards.push_back(new Card(16, 0));

    // shuffle them
    this->shuffleCards();

}

//distributes a specific amount of cards to a player
bool DrawPile::distributeCards(Player *player, int num_cards, std::string &err) {
    if (_cards.size() >= num_cards) {
        for (unsigned int i = 0; i < num_cards; ++i) {
            auto next_card = _cards.back();
            if (player->add_card(next_card, err)) {
                _cards.pop_back();
            } else {
                return false;
            }
        }
        return true;
    } else {
        err = "Could not distribute cards because draw pile has not enough cards.";
    }
    return false;
}

#endif

void DrawPile::write_into_json(rapidjson::Value &json, rapidjson::MemoryPoolAllocator<rapidjson::CrtAllocator> &allocator) const {
    unique_serializable::write_into_json(json, allocator);
    json.AddMember("cards", vector_utils::serialize_vector(_cards, allocator), allocator);
}

DrawPile *DrawPile::from_json(const rapidjson::Value &json) {
    if (json.HasMember("id") && json.HasMember("cards")) {
        std::vector<Card*> deserialized_cards = std::vector<Card*>();
        for (auto &serialized_card : json["cards"].GetArray()) {
            deserialized_cards.push_back(Card::from_json(serialized_card.GetObject()));
        }
        return new DrawPile(json["id"].GetString(), deserialized_cards);
    } else {
       throw SkullkingException("Could not parse DrawPile from json. 'id' or 'cards' were missing.");
    }
}