#ifndef SkullKing_PLAYEDPILE_H
#define SkullKing_PLAYEDPILE_H

#include <string>
#include <vector>
#include "Card.h"
#include "../player/Player.h"
#include "../../serialization/serializable.h"
#include "../../serialization/serializable_value.h"
#include "../../../../rapidjson/include/rapidjson/document.h"

class PlayedPile: public unique_serializable {
private:
    std::vector<Card*> _cards;
    serializable_value<int>* _currentType;

    explicit PlayedPile(std::string id);
public:
    PlayedPile();
    PlayedPile(std::string id, std::vector<Card*>& cards, serializable_value<int>* currentType);
    ~PlayedPile();

// accessors
    std::vector<Card*> get_cards() const;
    bool can_play(Player* player, const Card* card);
    std::pair<int,int> determine_winner_of_trick();
    int get_currentType() const noexcept;

#ifdef SKULLKING_SERVER
// state update functions
    void reset(std::string& err);  // Clears the stack
    bool try_play(const std::string& card_id, Card* played_card, Player* player, std::string& err);
#endif

// serializable interface
    virtual void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const override;
    static PlayedPile* from_json(const rapidjson::Value& json);
};

#endif //SkullKing_PLAYEDPILE_H