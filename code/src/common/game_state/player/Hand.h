#ifndef SkullKing_HAND_H
#define SkullKing_HAND_H

#include <vector>
#include "../../../../rapidjson/include/rapidjson/document.h"
#include "../cards/Card.h"

class Hand : public unique_serializable {

private:
    std::vector<Card*> _cards;
    Hand(std::string id);
    Hand(std::string id, std::vector<Card*> cards);
    Card* remove_card(std::vector<Card*>::iterator pos);
    Card* remove_card(int idx);
    Card* remove_card(Card* card);

public:
    Hand();
    ~Hand();

// accessors
    int get_nof_cards() const;
    const std::vector<Card*> get_cards() const;
    bool try_get_card(const std::string& card_id) const;

#ifdef SKULLKING_SERVER
// state update functions
    void setup_round(std::string& err);
    bool add_card(Card* card, std::string& err);
    bool remove_card(std::string card_id, Card*& played_card, std::string& err);
#endif

    std::vector<Card*>::iterator get_card_iterator();
    
// serializable interface
    static Hand* from_json(const rapidjson::Value& json);
    virtual void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const override;
};


#endif //SkullKing_HAND_H
