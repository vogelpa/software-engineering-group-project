#include "Player.h"

#include <utility>

#include "../../exceptions/SkullKingException.h"

Player::Player(std::string name) : unique_serializable() {
    this->_player_name = new serializable_value<std::string>(std::move(name));
    this->_bid = new serializable_value<int>(-1);
    this->_tricks = new serializable_value<int>(0);
    this->_score = new serializable_value<int>(0);
    this->_hand = new Hand();
    this->_bonus_points = new serializable_value<int>(0);
}

Player::Player(std::string id, serializable_value<std::string>* name,
               serializable_value<int>* score, Hand *hand, 
               serializable_value<int>* bid, serializable_value<int>* tricks,
               serializable_value<int>* bonus_points):
        unique_serializable(std::move(id)),
        _player_name(name),
        _hand(hand),
        _score(score),
        _bid(bid),
        _tricks(tricks),
        _bonus_points(bonus_points)
{ }

Player::~Player() {
    if (_player_name != nullptr) {
        delete _hand;
        delete _player_name;
        delete _score;
        delete _bid;
        delete _tricks;
        delete _bonus_points;

        _hand = nullptr;
        _player_name = nullptr;
        _score = nullptr;
        _bid = nullptr;
        _tricks = nullptr;
        _bonus_points = nullptr;
    }
}

#ifdef SKULLKING_SERVER

Player::Player(std::string id, std::string name) :
        unique_serializable(id)
{
    this->_player_name = new serializable_value<std::string>(name);
    this->_bid = new serializable_value<int>(0);
    this->_tricks = new serializable_value<int>(-1);
    this->_bonus_points = new serializable_value<int>(0);
    this->_score = new serializable_value<int>(0);
    this->_hand = new Hand();
}

std::string Player::get_game_id() {
    return _game_id;
}

void Player::set_game_id(std::string game_id) {
    _game_id = std::move(game_id);
}
#endif

int Player::get_score() const noexcept {
    return _score->get_value();
}

std::string Player::get_player_name() const noexcept {
    return this->_player_name->get_value();
}

const Hand* Player::get_hand() const noexcept {
    return this->_hand;
}

int Player::get_bid() const noexcept {
    return _bid->get_value();
}

int Player::get_tricks() const noexcept {
    return _tricks->get_value();
}

int Player::get_bonus_points() const noexcept {
    return _bonus_points->get_value();
}

int Player::get_nof_cards() const noexcept {
    return _hand->get_nof_cards();
}


#ifdef SKULLKING_SERVER
void Player::setup_round(std::string& err) {
    _hand->setup_round(err);
    _bid->set_value(-1);
    _tricks->set_value(0);
    _bonus_points->set_value(0);
}

//computes new scores
void Player::wrap_up_round(int round_counter, std::string &err) {
    int new_score = _score->get_value();
    int tricks = get_tricks();
    int bid = get_bid();

    if (bid == 0 && tricks == 0){
        new_score += 10*round_counter;
    }
    else if(bid == 0 && tricks > 0){
        new_score -= 10*round_counter;
    }
    else if (bid == tricks){
        new_score += 20*tricks + get_bonus_points();
    }
    else{
        new_score -= 10*std::abs(bid - tricks);
    }
    _score->set_value(new_score);
}

bool Player::placeBid(int new_bid, int round_counter, std::string &err){
    //bid must be within certain range
    if(new_bid < 0 || round_counter < new_bid){
        err = "Player has chosen an invalid bet";
        return false;
    }
    _bid->set_value(new_bid);
    return true;
}

bool Player::add_card(Card *card, std::string &err) {
    return _hand->add_card(card, err);
}

bool Player::remove_card(std::string card_id, Card*& card, std::string &err) {
    card = nullptr;
    return _hand->remove_card(card_id, card, err);
}

void Player::increment_tricks() {
    _tricks->set_value(get_tricks() + 1);
}

void Player::set_bonus_points(int bonus_points) {
    _bonus_points->set_value(bonus_points);
}
#endif

void Player::write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const {
    unique_serializable::write_into_json(json, allocator);

    rapidjson::Value id_val(_id.c_str(), allocator);
    json.AddMember("id", id_val, allocator);

    rapidjson::Value name_val(rapidjson::kObjectType);
    _player_name->write_into_json(name_val, allocator);
    json.AddMember("player_name", name_val, allocator);

    rapidjson::Value score_val(rapidjson::kObjectType);
    _score->write_into_json(score_val, allocator);
    json.AddMember("score", score_val, allocator);

    rapidjson::Value hand_val(rapidjson::kObjectType);
    _hand->write_into_json(hand_val, allocator);
    json.AddMember("hand", hand_val, allocator);

    rapidjson::Value bid_val(rapidjson::kObjectType);
    _bid->write_into_json(bid_val, allocator);
    json.AddMember("bid", bid_val, allocator);

    rapidjson::Value tricks_val(rapidjson::kObjectType);
    _tricks->write_into_json(tricks_val, allocator);
    json.AddMember("tricks", tricks_val, allocator);

    rapidjson::Value bonus_points_val(rapidjson::kObjectType);
    _bonus_points->write_into_json(bonus_points_val, allocator);
    json.AddMember("bonus_points", bonus_points_val, allocator);
}

void Player::write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator, Player* Otherplayer) const{
    unique_serializable::write_into_json(json, allocator);
    if(Otherplayer->get_id()==_id){
        write_into_json(json,allocator);
    }
    else{
    rapidjson::Value id_val(_id.c_str(), allocator);
    json.AddMember("id", id_val, allocator);

    rapidjson::Value name_val(rapidjson::kObjectType);
    _player_name->write_into_json(name_val, allocator);
    json.AddMember("player_name", name_val, allocator);

    rapidjson::Value score_val(rapidjson::kObjectType);
    _score->write_into_json(score_val, allocator);
    json.AddMember("score", score_val, allocator);

    rapidjson::Value bid_val(rapidjson::kObjectType);
    _bid->write_into_json(bid_val, allocator);
    json.AddMember("bid", bid_val, allocator);

    rapidjson::Value tricks_val(rapidjson::kObjectType);
    _tricks->write_into_json(tricks_val, allocator);
    json.AddMember("tricks", tricks_val, allocator);
    }
}

Player* Player::from_json(const rapidjson::Value &json) {
    if (json.HasMember("id")
        && json.HasMember("player_name")
        && json.HasMember("score")
        && json.HasMember("hand")
        && json.HasMember("bid")
        && json.HasMember("tricks")
        && json.HasMember("bonus_points"))
    {
        return new Player(
                json["id"].GetString(),
                serializable_value<std::string>::from_json(json["player_name"].GetObject()),
                serializable_value<int>::from_json(json["score"].GetObject()),
                Hand::from_json(json["hand"].GetObject()),
                serializable_value<int>::from_json(json["bid"].GetObject()),
                serializable_value<int>::from_json(json["tricks"].GetObject()),
                serializable_value<int>::from_json(json["bonus_points"].GetObject()));
    } 
    else if (
        json.HasMember("id")
        && json.HasMember("player_name")
        && json.HasMember("score")
        && json.HasMember("bid")
        && json.HasMember("tricks")
    ){
                return new Player(
                json["id"].GetString(),
                serializable_value<std::string>::from_json(json["player_name"].GetObject()),
                serializable_value<int>::from_json(json["score"].GetObject()),
                new Hand(),
                serializable_value<int>::from_json(json["bid"].GetObject()),
                serializable_value<int>::from_json(json["tricks"].GetObject()),
                0);
    }
        else {
       throw SkullkingException("Failed to deserialize Player from json. Required json entries were missing.");
    }
}
