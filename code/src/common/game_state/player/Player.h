#ifndef SkullKing_PLAYER_H
#define SkullKing_PLAYER_H


#include <string>
#include "Hand.h"
#include "../../serialization/uuid_generator.h"
#include "../../../../rapidjson/include/rapidjson/document.h"
#include "../../serialization/unique_serializable.h"
#include "../../serialization/serializable_value.h"

class Player : public unique_serializable {
private:
    serializable_value<std::string>* _player_name;
    serializable_value<int>* _score;
    Hand* _hand;
    serializable_value<int>* _bid;
    serializable_value<int>* _tricks;
    serializable_value<int>* _bonus_points;

#ifdef SKULLKING_SERVER
    std::string _game_id;
#endif

    /*
     * Deserialization constructor
     */
    Player(std::string id,
           serializable_value<std::string>* name,
           serializable_value<int>* score,
           Hand* hand,
           //added 8.4
           serializable_value<int>* bid,
           serializable_value<int>* tricks,
           serializable_value<int>* bonus_points
           );

    // Player(std::string id,
    //        serializable_value<std::string>* name,
    //        serializable_value<int>* score,
    //        //added 8.4
    //        serializable_value<int>* bid,
    //        serializable_value<int>* tricks,
    //        serializable_value<int>* bonus_points
    //        );

public:
// constructors
    explicit Player(std::string name);   // for client
    ~Player();

#ifdef SKULLKING_SERVER
    Player(std::string id, std::string name);  // for server

    std::string get_game_id();
    void set_game_id(std::string game_id);
#endif

    // accessors
    int get_score() const noexcept;
    int get_bid() const noexcept;
    int get_tricks() const noexcept;
    int get_bonus_points() const noexcept;
    int get_nof_cards() const noexcept;
    const Hand* get_hand() const noexcept;
    std::string get_player_name() const noexcept;
   


#ifdef SKULLKING_SERVER
    // state update functions

    //added 8.4
    bool placeBid(int new_bid, int round_counter, std::string& err);

    //added 13.4.,
    void increment_tricks();
    void set_bonus_points(int bonus_points);
    
    bool add_card(Card* card, std::string& err);
    bool remove_card(std::string card_id, Card*& card, std::string& err);

    //edited 11.4
    void wrap_up_round(int round_counter, std::string& err);
    void setup_round(std::string& err);
#endif


    // serialization
    static Player* from_json(const rapidjson::Value& json);
    virtual void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const override;
    virtual void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator, Player* Otherplayer) const;

};


#endif //SkullKing_PLAYER_H