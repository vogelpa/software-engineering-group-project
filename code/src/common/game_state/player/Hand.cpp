#include "Hand.h"

#include "../../exceptions/SkullKingException.h"
#include "../../serialization/vector_utils.h"

Hand::Hand() : unique_serializable() { }

Hand::Hand(std::string id) : unique_serializable(id) { }

// deserialization constructor
Hand::Hand(std::string id, std::vector<Card*> cards) : unique_serializable(id) {
    this->_cards = cards;
}

Hand::~Hand() {
    for (int i = 0; i < _cards.size(); i++) {
        delete _cards.at((i));
        _cards.at(i) = nullptr;
    }
    _cards.clear();
}

int Hand::get_nof_cards() const {
    return _cards.size();
}

const std::vector<Card*> Hand::get_cards() const {
    return _cards;
}

bool Hand::try_get_card(const std::string &card_id) const {
    auto it = std::find_if(_cards.begin(), _cards.end(),
                           [&card_id](const Card* x) { return x->get_id() == card_id;});
    Card* found_card = *it;
    if (found_card->get_id() == card_id) {
        return true;
    }
    return false;
}

Card* Hand::remove_card(int idx) {
    return remove_card(_cards.begin() + idx);
}

Card* Hand::remove_card(Card* card) {
    auto pos = std::find(_cards.begin(), _cards.end(), card);
    return remove_card(pos);
}

Card* Hand::remove_card(std::vector<Card*>::iterator pos) {
    if (pos >= _cards.begin() && pos < _cards.end()) {
        Card* res = *pos;
        _cards.erase(pos);
        return res;
    }
    return nullptr;
}

std::vector<Card*>::iterator Hand::get_card_iterator() {
    return _cards.begin();
}


#ifdef SKULLKING_SERVER

// removes all cards (if any) from hand and clears it
void Hand::setup_round(std::string &err) {
    for (int i = 0; i < _cards.size(); i++) {
        delete _cards[i];
    }
    _cards.clear();
}

bool Hand::add_card(Card* new_card, std::string &err) {
    _cards.push_back(new_card);
    return true;
}

bool Hand::remove_card(std::string card_id, Card*& played_card, std::string &err) {
    played_card = nullptr;
    auto it = std::find_if(_cards.begin(), _cards.end(),
                           [&card_id](const Card* x) { return x->get_id() == card_id;});
    if (it < _cards.end()) {
        played_card = remove_card(it);
        return true;
    } else {
        err = "Could not play Card, as the requested Card was not on the player's Hand.";
        return false;
    }
}
#endif


void Hand::write_into_json(rapidjson::Value &json, rapidjson::Document::AllocatorType& allocator) const {
    unique_serializable::write_into_json(json, allocator);
    json.AddMember("cards", vector_utils::serialize_vector(_cards, allocator), allocator);
}

Hand *Hand::from_json(const rapidjson::Value &json) {
    if (json.HasMember("id") && json.HasMember("cards")) {
        std::vector<Card*> deserialized_cards = std::vector<Card*>();
        for (auto &serialized_card : json["cards"].GetArray()) {
            deserialized_cards.push_back(Card::from_json(serialized_card.GetObject()));
        }
        return new Hand(json["id"].GetString(), deserialized_cards);
    } else {
       throw SkullkingException("Could not parse hand from json. 'cards' were missing.");
    }
}

