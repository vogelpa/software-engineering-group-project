#include "GameState.h"

#include <utility>

#include "../exceptions/SkullKingException.h"
#include "../serialization/vector_utils.h"

GameState::GameState() : unique_serializable() {
    this->_draw_pile = new DrawPile();
    this->_played_pile = new PlayedPile();
    this->_players = std::vector<Player*>();
    this->_is_started = new serializable_value<bool>(false);
    this->_is_finished = new serializable_value<bool>(false);
    this->_current_player_idx = new serializable_value<int>(0);
    this->_play_direction = new serializable_value<int>(1);
    this->_round_counter = new serializable_value<int>(0);
    this->_starting_player_idx = new serializable_value<int>(0);
    this->_roundstart_player_idx = new serializable_value<int>(0);
    this->_players_to_remove = std::vector<Player*>();
    this->_scary_mary = {new Card(0,0), new Card(15,0)};
}

GameState::GameState(std::string id, DrawPile *draw_pile, PlayedPile *discard_pile,
                       std::vector<Player *> &players, serializable_value<bool> *is_started,
                       serializable_value<bool> *is_finished, serializable_value<int> *current_player_idx,
                       serializable_value<int> *play_direction, serializable_value<int>* round_counter, 
                       serializable_value<int> *starting_player_idx, serializable_value<int> *roundstart_player_idx,
                       std::vector<Player*> &players_to_remove, std::vector<Card*> &scary_mary)
        : unique_serializable(std::move(id)),
          _draw_pile(draw_pile),
          _played_pile(discard_pile),
          _players(players),
          _is_started(is_started),
          _is_finished(is_finished),
          _current_player_idx(current_player_idx),
          _play_direction(play_direction),
          _round_counter(round_counter),
          _starting_player_idx(starting_player_idx),
          _roundstart_player_idx(roundstart_player_idx),
          _players_to_remove(players_to_remove),
          _scary_mary(scary_mary)
       { }

GameState::GameState(std::string id) : unique_serializable(std::move(id)) {
    this->_draw_pile = new DrawPile();
    this->_played_pile = new PlayedPile();
    this->_players = std::vector<Player*>();
    this->_is_started = new serializable_value<bool>(false);
    this->_is_finished = new serializable_value<bool>(false);
    this->_current_player_idx = new serializable_value<int>(0);
    this->_play_direction = new serializable_value<int>(1);
    this->_round_counter = new serializable_value<int>(0);
    this->_starting_player_idx = new serializable_value<int>(0);
    this->_roundstart_player_idx = new serializable_value<int>(0);
    this->_players_to_remove = std::vector<Player*>();
    this->_scary_mary = {new Card(0,0), new Card(15,0)};
}

GameState::~GameState() {
    if (_is_started != nullptr) {
        delete _is_started;
        delete _is_finished;
        delete _draw_pile;
        delete _played_pile;
        delete _current_player_idx;
        delete _starting_player_idx;
        delete _roundstart_player_idx;
        delete _play_direction;
        delete _round_counter;

        _is_started = nullptr;
        _is_finished = nullptr;
        _draw_pile = nullptr;
        _played_pile = nullptr;
        _current_player_idx = nullptr;
        _starting_player_idx = nullptr;
        _roundstart_player_idx = nullptr;
        _play_direction = nullptr;
        _round_counter = nullptr;
    }
}

// accessors
Player* GameState::get_current_player() const {
    if(_current_player_idx == nullptr || _players.empty()) {
        return nullptr;
    }
    return _players[_current_player_idx->get_value()];
}
int GameState::get_starting_player_index() const {
    return _starting_player_idx->get_value();
}

PlayedPile *GameState::get_played_pile() const {
    return _played_pile;
}

std::vector<Card*>& GameState::get_scary_mary(){
    return _scary_mary;
}

DrawPile *GameState::get_draw_pile() const {
    return _draw_pile;
}

bool GameState::is_full() const {
    return _players.size() == _max_nof_players;
}

bool GameState::is_started() const {
    return _is_started->get_value();
}

bool GameState::is_finished() const {
    return _is_finished->get_value();
}

bool GameState::is_trick_finished() const {
    return (_current_player_idx->get_value() == _starting_player_idx->get_value())
            && (!_played_pile->get_cards().empty());
}

int GameState::get_round_counter() const {
    return _round_counter->get_value();
}

//checks if all bids are placed
bool GameState::are_bids_placed() const{
    for(auto _player : _players){
        if(_player->get_bid() == -1){
            return false;
        }
    }
    return true;
}

int GameState::get_player_index(Player *player) const {
    auto it = std::find(_players.begin(), _players.end(), player);
    if (it == _players.end()) {
        return -1;
    } else {
        return it - _players.begin();
    }
}

bool GameState::is_player_in_game(Player *player) const {
    return std::find(_players.begin(), _players.end(), player) < _players.end();
}

bool GameState::is_allowed_to_play_now(Player *player) const {
    return player == get_current_player();
}

std::vector<Player*>& GameState::get_players() {
    return _players;
}

std::vector<Player*>& GameState::get_players_to_remove() {
    return _players_to_remove;
}

void GameState::add_player_to_remove(Player* player) {
    _players_to_remove.push_back(player);
}

void GameState::erase_player(Player* player) {
    delete player;
    _players.erase(_players.begin() + get_player_index(player));
    //end game if too few players in game (except when in lobby)
    if (_players.size() < _min_nof_players && is_started()){
        _is_finished->set_value(true);
    }
}

#ifdef SKULLKING_SERVER

// state modification functions without diff
void GameState::setup_round(std::string &err) {

    // update round number initial value is 0
    _round_counter->set_value(get_round_counter()+1);

    // setup DrawPile
    _draw_pile->setup_game(err);

    // setup PlayedPile
    _played_pile->reset(err);

    // setup players
    for (auto & _player : _players) {
        _player->setup_round(err);
        
        //edited, give every player amount of round cards, in case an error occurs, display error
        if(!_draw_pile->distributeCards(_player, get_round_counter(), err)){
            std::cerr << err << std::endl;
        }
    }
    _starting_player_idx->set_value(_roundstart_player_idx->get_value() % _players.size());
    _current_player_idx->set_value(_starting_player_idx->get_value());
}

void GameState::wrap_up_trick(std::string& err) {

    //determine winner of trick and bonus points
    std::pair<int,int> winner_and_bonus = _played_pile->determine_winner_of_trick();
    int winner_of_trick = winner_and_bonus.first;
    int bonus_points = winner_and_bonus.second;
 
    //transform winner_of_trick to corresponding index
    winner_of_trick = (_starting_player_idx->get_value() + winner_of_trick) % _players.size();

    //update bids won of winner of trick
    _players[winner_of_trick]->increment_tricks();

    //set potential bous points (max. once per round)
    _players[winner_of_trick]->set_bonus_points(bonus_points);

    //set winner of trick to current/starting player
    _starting_player_idx->set_value(winner_of_trick);
    _current_player_idx->set_value(winner_of_trick);

    _played_pile->reset(err);

    remove_players(err);

    //if a player has no cards left, all players have no cards left -> wrap up round
    if(_players[0]->get_nof_cards() == 0){
        wrap_up_round(err);
    }
}

void GameState::wrap_up_round(std::string& err) {
    for (int i = 0; i < _players.size(); i++){
        _players[i]->wrap_up_round(get_round_counter(), err);
    }
    if (get_round_counter() < 10){
        _roundstart_player_idx->set_value(_roundstart_player_idx->get_value() + 1);
        setup_round(err);
    } else{
        this->_is_finished->set_value(true);
    }
}

bool GameState::start_game(std::string &err) {
    if (_players.size() < _min_nof_players) {
        err = "You need at least " + std::to_string(_min_nof_players) + " players to start the game.";
        return false;
    }

    if (!is_started()) {
        this->setup_round(err);
        _is_started->set_value(true);
        return true;
    } else {
        err = "Could not start game, as the game was already started";
        return false;
    }
}

//sets the bid of player to chosen value
bool GameState::placeBid(Player* player, int bid, std::string& err) const {
    if(!is_started()){
        err = "Game hasn't started yet.";
        return false;
    }
    if (!is_player_in_game(player)){
        err = "Player is not part of the game.";
        return false;
    }
    if(!player->placeBid(bid, get_round_counter(), err)){
        return false;
    }
    return true;
}

//removes player from game at any time (in lobby, before playing a card, after playing a card)
bool GameState::remove_player(Player *player_ptr, std::string &err) {
    int remove_idx = get_player_index(player_ptr);
    int current_idx = _current_player_idx->get_value();
    int starting_idx = _starting_player_idx->get_value();

    if (remove_idx != -1) {
        //if the removing player has already played a card, wait with removing until the trick has ended
        if ((starting_idx < current_idx && (remove_idx >= starting_idx && remove_idx < current_idx)) ||
            (starting_idx > current_idx && (remove_idx >= starting_idx || remove_idx < current_idx))) {
            add_player_to_remove(_players[remove_idx]);
            return true;
        }
        //if current player is to be removed and is the last in vector, set current player to first one
        if (remove_idx == _players.size()-1 && remove_idx == current_idx){
            _current_player_idx->set_value(0);
        }
        //dito before
        if (remove_idx == _players.size()-1 && remove_idx == starting_idx){
            _starting_player_idx->set_value(0);
        }

        erase_player(_players[remove_idx]);
        //in case he's the last player of the trick/round
        if (remove_idx == current_idx && (remove_idx + 1) % (_players.size() + 1) == starting_idx){
            //we can't call wrap_up_trick if the game is finished or not started yet
            if(!is_finished() && is_started()) {
                wrap_up_trick(err);
            }
        }
        return true;
    }
    else {
        err = "Could not remove player as it was not found in the game.";
        return false;
    }
}

//removes players from the players_to_remove vector
void GameState::remove_players(std::string &err) {
    //sort players_to_remove vector highest to lowes index
    std:sort(_players_to_remove.begin(), _players_to_remove.end(), [this](Player* p1, Player* p2){return get_player_index(p1) > get_player_index(p2);});
    if (!_players_to_remove.empty()) {
        for (int i = 0; i <_players_to_remove.size(); i++) {
            //if a removing player has started a round at least once, we need to adapt the round starting player (otherwhise people get skipped or can start a round twice in a row)
            if (get_round_counter() >= get_player_index(_players_to_remove[i])){
                _roundstart_player_idx->set_value(_roundstart_player_idx->get_value() - 1);
            }
        }
        for (int i = 0; i <_players_to_remove.size(); i++) {
            erase_player(_players_to_remove[i]);
        }
        _players_to_remove.clear();
    }
    //end game (if too few players left to play)
    if (is_finished()){
        for (auto& p: _players){
            p->wrap_up_round(10,err);
        }
    }
}

//adds player to game (we allow same names)
bool GameState::add_player(Player* player_ptr, std::string& err) {
    if (is_started()) {
        err = "Could not join game, because the requested game is already started.";
        return false;
    }
    if (is_finished()) {
        err = "Could not join game, because the requested game is already finished.";
        return false;
    }
    if (_players.size() >= _max_nof_players) {
        err = "Could not join game, because the max number of players is already reached.";
        return false;
    }
    if (std::find(_players.begin(), _players.end(), player_ptr) != _players.end()) {
        err = "Could not join game, because this Player is already part of this game.";
        return false;
    }

    _players.push_back(player_ptr);
    return true;
}

bool GameState::play_card(Player *Player, const std::string& card_id, std::string &err) {
    if (!is_player_in_game(Player)) {
        err = "Server refused to perform play_card. Player is not part of the game.";
        return false;
    }
    if (!is_allowed_to_play_now(Player)) {
        err = "It's not your turn yet.";
        return false;
    }
    if (is_finished()) {
        err = "Could not play Card, because the requested game is already finished.";
        return false;
    }
    if (!are_bids_placed()) {
        err = "You must wait until all players have placed their bid.";
        return false;
    }
    //initialize two new cards, this is needed for scary mary
    Card* play_card = new Card(0,0);
    Card* new_play_card = new Card(0,0);
    std::string new_card_id = card_id;
    ScaryMary(Player, play_card, new_play_card, card_id, new_card_id);

    if (Player->get_hand()->try_get_card(new_card_id)) {
        if (_played_pile->try_play(card_id, play_card, Player, err)) {
            int current_player = _current_player_idx->get_value();
            _current_player_idx->set_value((current_player + 1) % _players.size());
        }
        else{
            return false;
        }
        if (Player->remove_card(new_card_id, new_play_card, err)) {
            return true;
        }
    }
    err = "You don't possess the card with id: " + card_id;
    return false;
}

//finds the card to be removed from the players hand and the one to be played onto the pile
// (they are the same except when a scary mary is played)
void GameState::ScaryMary(Player* Player, Card*& play_card, Card*& new_play_card, const std::string& card_id, std::string& new_card_id){
    for (auto& it : Player->get_hand()->get_cards()){
        if (it->get_id() == card_id){
            play_card = it;
        }
    }
    if (card_id == _scary_mary[0]->get_id() || card_id == _scary_mary[1]->get_id()){
        //check if we have scary mary
        for(auto& c : Player->get_hand()->get_cards()){
            if (c->get_value() == 17){
                new_card_id = c->get_id();
                new_play_card = c;
            }
        }

        //create new (disposable) card, that can be deleted when the pile resets
        if (card_id == _scary_mary[0]->get_id()){
            play_card = new Card (0,0);
        }
        else if (card_id == _scary_mary[1]->get_id()){
            play_card = new Card(15, 0);
        }
    }
}

#endif


// Serializable interface
void GameState::write_into_json(rapidjson::Value &json,
                                 rapidjson::MemoryPoolAllocator<rapidjson::CrtAllocator> &allocator, Player* otherPlayer) const {
    unique_serializable::write_into_json(json, allocator);

    rapidjson::Value is_finished_val(rapidjson::kObjectType);
    _is_finished->write_into_json(is_finished_val, allocator);
    json.AddMember("is_finished", is_finished_val, allocator);

    rapidjson::Value is_started_val(rapidjson::kObjectType);
    _is_started->write_into_json(is_started_val, allocator);
    json.AddMember("is_started", is_started_val, allocator);

    rapidjson::Value current_player_idx_val(rapidjson::kObjectType);
    _current_player_idx->write_into_json(current_player_idx_val, allocator);
    json.AddMember("current_player_idx", current_player_idx_val, allocator);

    rapidjson::Value play_direction_val(rapidjson::kObjectType);
    _play_direction->write_into_json(play_direction_val, allocator);
    json.AddMember("play_direction", play_direction_val, allocator);

    rapidjson::Value starting_player_idx_val(rapidjson::kObjectType);
    _starting_player_idx->write_into_json(starting_player_idx_val, allocator);
    json.AddMember("starting_player_idx", starting_player_idx_val, allocator);

    rapidjson::Value roundstart_player_idx_val(rapidjson::kObjectType);
    _roundstart_player_idx->write_into_json(roundstart_player_idx_val, allocator);
    json.AddMember("roundstart_player_idx", roundstart_player_idx_val, allocator);

    rapidjson::Value round_counter_val(rapidjson::kObjectType);
    _round_counter->write_into_json(round_counter_val, allocator);
    json.AddMember("round_counter", round_counter_val, allocator);

    rapidjson::Value discard_pile_val(rapidjson::kObjectType);
    _played_pile->write_into_json(discard_pile_val, allocator);
    json.AddMember("PlayedPile", discard_pile_val, allocator);

    
    rapidjson::Value arr_val(rapidjson::kArrayType);
        for (auto i : _players) {
            rapidjson::Value elem(rapidjson::kObjectType);
            i->write_into_json(elem, allocator,otherPlayer);
            arr_val.PushBack(elem, allocator);

        }
    json.AddMember("players", arr_val, allocator);

    rapidjson::Value arr_val2(rapidjson::kArrayType);
        for (auto i : _players_to_remove) {
            rapidjson::Value elem2(rapidjson::kObjectType);
            i->write_into_json(elem2, allocator);
            arr_val2.PushBack(elem2, allocator);
        }
    json.AddMember("players_to_remove", arr_val2, allocator);
    json.AddMember("scary_mary", vector_utils::serialize_vector(_scary_mary, allocator), allocator);
}



GameState* GameState::from_json(const rapidjson::Value &json) {
    if (json.HasMember("id")
        && json.HasMember("is_finished")
        && json.HasMember("is_started")
        && json.HasMember("current_player_idx")
        && json.HasMember("round_counter")
        && json.HasMember("starting_player_idx")
        && json.HasMember("roundstart_player_idx")
        && json.HasMember("players")
        && json.HasMember("PlayedPile")
        && json.HasMember("players_to_remove")
        && json.HasMember("scary_mary")
        )
    {
        std::vector<Player*> deserialized_players;
        for (auto &serialized_player : json["players"].GetArray()) {
            deserialized_players.push_back(Player::from_json(serialized_player.GetObject()));
        }

        std::vector<Player*> deserialized_players_to_remove;
        for (auto &serialized_player : json["players_to_remove"].GetArray()) {
            deserialized_players_to_remove.push_back(Player::from_json(serialized_player.GetObject()));
        }

        std::vector<Card*> deserialized_scary_mary = std::vector<Card*>();
        for (auto &serialized_card : json["scary_mary"].GetArray()) {
            deserialized_scary_mary.push_back(Card::from_json(serialized_card.GetObject()));
        }

        return new GameState(json["id"].GetString(),
                              new DrawPile(),
                              PlayedPile::from_json(json["PlayedPile"].GetObject()),
                              deserialized_players,
                              serializable_value<bool>::from_json(json["is_started"].GetObject()),
                              serializable_value<bool>::from_json(json["is_finished"].GetObject()),
                              serializable_value<int>::from_json(json["current_player_idx"].GetObject()),
                              0,
                              serializable_value<int>::from_json(json["round_counter"].GetObject()),
                              serializable_value<int>::from_json(json["starting_player_idx"].GetObject()),
                              serializable_value<int>::from_json(json["roundstart_player_idx"].GetObject()),
                              deserialized_players_to_remove,
                              deserialized_scary_mary);
    } else {
       throw SkullkingException("Failed to deserialize GameSate. Required entries were missing.");
    }
}