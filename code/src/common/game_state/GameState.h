// 13.4. ready for testing


#ifndef SkullKing_GAMESTATE_H
#define SkullKing_GAMESTATE_H


#include <vector>
#include <string>
#include "../../../rapidjson/include/rapidjson/document.h"
#include "player/Player.h"
#include "cards/DrawPile.h"
#include "cards/PlayedPile.h"
#include "../serialization/serializable.h"
#include "../serialization/serializable_value.h"
#include "../serialization/unique_serializable.h"

class GameState : public unique_serializable {
private:

    static const int _max_nof_players = 6;
    static const int _min_nof_players = 2;

    std::vector<Player*> _players;
    DrawPile* _draw_pile;
    PlayedPile* _played_pile;
    serializable_value<bool>* _is_started;
    serializable_value<bool>* _is_finished;
    serializable_value<int>* _round_counter;
    serializable_value<int>* _current_player_idx;
    serializable_value<int>* _play_direction;  // 1 or -1 depending on which direction is played in
    serializable_value<int>* _starting_player_idx;
    serializable_value<int>* _roundstart_player_idx;

    //added 4.5
    std::vector<Player*> _players_to_remove;
    std::vector<Card*> _scary_mary;
    // from_diff constructor
    explicit GameState(std::string id);

    // deserialization constructor
    GameState(
            std::string id,
            DrawPile* draw_pile,
            PlayedPile* discard_pile,
            std::vector<Player*>& players,
            serializable_value<bool>* is_started,
            serializable_value<bool>* is_finished,
            serializable_value<int>* current_player_idx,
            serializable_value<int>* play_direction,
            serializable_value<int>* round_counter,
            serializable_value<int>* starting_player_idx,
            serializable_value<int>* _roundstart_player_idx,
            std::vector<Player*>& players_to_remove,
            std::vector<Card*>& _scary_mary);


public:
    GameState();
    ~GameState();

// accessors
    bool is_full() const;
    bool is_started() const;
    bool is_finished() const;
    bool is_trick_finished() const;
    bool is_player_in_game(Player* player) const;
    bool is_allowed_to_play_now(Player* player) const;
    std::vector<Player*>& get_players();
    int get_round_counter() const;
    bool are_bids_placed() const;
    DrawPile* get_draw_pile() const;
    PlayedPile* get_played_pile() const;
    std::vector<Card*>& get_scary_mary();
    Player* get_current_player() const;
    std::vector<Player*>& get_players_to_remove();
    void add_player_to_remove(Player* player);
    void erase_player(Player* player);
    int get_player_index(Player* player) const;
    int get_starting_player_index() const;

#ifdef SKULLKING_SERVER
// server-side state update functions
    void setup_round(std::string& err);   // server side initialization
    bool remove_player(Player* player, std::string& err);
    void remove_players(std::string& err);
    bool add_player(Player* player, std::string& err);
    bool start_game(std::string& err);
    bool play_card(Player* player, const std::string& card_id, std::string& err);
    bool placeBid(Player* player, int bid, std::string& err) const;
    void ScaryMary(Player* Player, Card*& play_card, Card*& new_play_card, const std::string& card_id, std::string& new_card_id);

    //end of trick functions
    void wrap_up_trick(std::string& err);
    void wrap_up_round(std::string& err);
#endif

// serializable interface
    static GameState* from_json(const rapidjson::Value& json);
    virtual void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator, Player* player) const;

};


#endif //SkullKing_GAMESTATE_H
