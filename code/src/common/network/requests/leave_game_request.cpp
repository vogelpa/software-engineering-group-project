#include "leave_game_request.h"


// Public constructor
leave_game_request::leave_game_request(std::string game_id, std::string player_id)
        : client_request(client_request::create_base_class_properties(RequestType::leave_game, uuid_generator::generate_uuid_v4(), player_id, game_id) )
        
{ }

// private constructor for deserialization
leave_game_request::leave_game_request(client_request::base_class_properties props) :
        client_request(props)
        
{ }

leave_game_request* leave_game_request::from_json(const rapidjson::Value& json) {
    base_class_properties props = client_request::extract_base_class_properties(json);
    if (json.HasMember("leave_game")) {
        return new leave_game_request(props);
    } else {
       throw SkullkingException("Could not find 'card_id' or 'value' in leave_game_request");
    }
}

void leave_game_request::write_into_json(rapidjson::Value &json,
                                        rapidjson::MemoryPoolAllocator<rapidjson::CrtAllocator> &allocator) const {
    client_request::write_into_json(json, allocator);
    json.AddMember("leave_game",true, allocator);
}
