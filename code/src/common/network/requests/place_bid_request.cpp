#include "place_bid_request.h"

// Public constructor
place_bid_request::place_bid_request(std::string game_id, std::string player_id, int bid_amount):
        client_request( client_request::create_base_class_properties(RequestType::place_bid, uuid_generator::generate_uuid_v4(), player_id, game_id) ),
        _bid_amount(bid_amount)
{ }

// private constructor for deserialization
place_bid_request::place_bid_request(client_request::base_class_properties props, int bid_amount) :
        client_request(props),
        _bid_amount(bid_amount)
{ }

place_bid_request* place_bid_request::from_json(const rapidjson::Value &json) {
    base_class_properties props = client_request::extract_base_class_properties(json);
    if (json.HasMember("bid")) {
        return new place_bid_request(props, json["bid"].GetInt());
    } else {
       throw SkullkingException("Could not find 'bid_amount' in place_bid request");
    }
}

void place_bid_request::write_into_json(rapidjson::Value &json,
                                        rapidjson::MemoryPoolAllocator<rapidjson::CrtAllocator> &allocator) const {
    client_request::write_into_json(json, allocator);
    json.AddMember("bid", _bid_amount,allocator);
}
