#ifndef SkullKing_FOLD_REQUEST_H
#define SkullKing_FOLD_REQUEST_H

#include <string>
#include "client_request.h"
#include "../../../../rapidjson/include/rapidjson/document.h"

class place_bid_request : public client_request{

private:
    int _bid_amount;
    /*
     * Private constructor for deserialization
     */
    explicit place_bid_request(base_class_properties, int bid_amount);

public:
    [[nodiscard]] int get_bid_amount() const { return this->_bid_amount; }
    place_bid_request(std::string game_id, std::string player_id, int bid_amount);
    virtual void write_into_json(rapidjson::Value& json, rapidjson::Document::AllocatorType& allocator) const override;
    static place_bid_request* from_json(const rapidjson::Value& json);
};

#endif //SkullKing_FOLD_REQUEST_H